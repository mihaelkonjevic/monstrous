import monstrous from '../index.js';

export function clean_snapshot(s) {
  s = s.replace(/\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}.\d{1,6}[+-]\d{2}/g, '{now}')
  s = s.replace(/\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.\d{3}Z/g, '{time}')
  s = s.replace(/[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}/g, '{uuid}')

  return s;
};

export function connect(tap, schema = 'general') {
  return async () => {
    const db = await monstrous({
      host: process.env.POSTGRES_HOST || 'localhost',
      port: 5432,
      database: 'monstrous',
      user: 'postgres',
      password: 'fancydbpassword',
      scripts: 'test/sql'
    });

    await db.execute(db.schema[schema]);
    await db.reload();

    tap.context.db = db;
  }
};

export function teardown(tap) {
  return async () => {
    tap.context.db.$pool.end();

    delete tap.context.db;
  }
};

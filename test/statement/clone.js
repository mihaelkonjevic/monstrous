import tap from 'tap';
import {connect, teardown, clean_snapshot} from '../util.js';

tap.cleanSnapshot = clean_snapshot;
tap.before(connect(tap));
tap.teardown(teardown(tap));

tap.test('join clones the previous statement', async t => {
  const statement1 = tap.context.db.join_parent;
  const statement2 = statement1.join(tap.context.db.join_junction);
  const statement3 = statement2.join(tap.context.db.join_ettin);

  t.matchSnapshot(tap.context.db.select(statement1, tap.context.db.$target.log));
  t.matchSnapshot(tap.context.db.select(statement2, tap.context.db.$target.log));
  t.matchSnapshot(tap.context.db.select(statement3, tap.context.db.$target.log));

  const results1 = await tap.context.db.select(statement1);
  const results2 = await tap.context.db.select(statement2);
  const results3 = await tap.context.db.select(statement3);

  t.equal(results1.length, 3);
  t.equal(results2.length, 3);
  t.equal(results3.length, 3);

  t.matchSnapshot(results1);
  t.matchSnapshot(results2);
  t.matchSnapshot(results3);
});

tap.only('filter clones the previous statement', async t => {
  const statement1 = tap.context.db.regular_table;
  const statement2 = statement1.filter({'bool is': true});
  const statement3 = statement2.filter(1);

  t.matchSnapshot(tap.context.db.select(statement1, tap.context.db.$target.log));
  t.matchSnapshot(tap.context.db.select(statement2, tap.context.db.$target.log));
  t.matchSnapshot(tap.context.db.select(statement3, tap.context.db.$target.log));

  const results1 = await tap.context.db.select(statement1);
  const results2 = await tap.context.db.select(statement2);
  const results3 = await tap.context.db.select(statement3);

  t.equal(results1.length, 5);
  t.equal(results2.length, 3);
  t.equal(results3.length, 1);

  t.matchSnapshot(results1);
  t.matchSnapshot(results2);
  t.matchSnapshot(results3);
});

tap.only('project clones the previous statement', async t => {
  const statement1 = tap.context.db.regular_table;
  const statement2 = statement1.project({
    $key: tap.context.db.regular_table.$id,
    $columns: [
      tap.context.db.regular_table.$num,
      tap.context.db.expr('1').as('one')
    ]
  });
  const statement3 = statement2.project({
    $key: tap.context.db.regular_table.$id,
    $columns: [
      tap.context.db.regular_table.$txt,
      tap.context.db.expr('2').as('two')
    ]
  });

  t.matchSnapshot(tap.context.db.select(statement1, tap.context.db.$target.log));
  t.matchSnapshot(tap.context.db.select(statement2, tap.context.db.$target.log));
  t.matchSnapshot(tap.context.db.select(statement3, tap.context.db.$target.log));

  const results1 = await tap.context.db.select(statement1);
  const results2 = await tap.context.db.select(statement2);
  const results3 = await tap.context.db.select(statement3);

  t.equal(results1.length, 5);
  t.equal(results2.length, 5);
  t.equal(results3.length, 5);

  t.match(results1[0], {
    id: Number,
    bool: Boolean,
    num: Number,
    txt: String,
    inserted_at: Date
  });
  t.match(results2[0], {
    num: Number
  });
  t.match(results3[0], {
    txt: String
  });

  t.matchSnapshot(results1);
  t.matchSnapshot(results2);
  t.matchSnapshot(results3);
});

tap.only('order clones the previous statement', async t => {
  const statement1 = tap.context.db.regular_table;
  const statement2 = statement1.order('id desc');
  const statement3 = statement2.order('id asc');

  t.matchSnapshot(tap.context.db.select(statement1, tap.context.db.$target.log));
  t.matchSnapshot(tap.context.db.select(statement2, tap.context.db.$target.log));
  t.matchSnapshot(tap.context.db.select(statement3, tap.context.db.$target.log));

  const results1 = await tap.context.db.select(statement1);
  const results2 = await tap.context.db.select(statement2);
  const results3 = await tap.context.db.select(statement3);

  t.matchSnapshot(results1);
  t.matchSnapshot(results2);
  t.matchSnapshot(results3);
});

tap.only('offset clones the previous statement', async t => {
  const statement1 = tap.context.db.regular_table;
  const statement2 = statement1.offset(4);
  const statement3 = statement2.offset(2);

  t.matchSnapshot(tap.context.db.select(statement1, tap.context.db.$target.log));
  t.matchSnapshot(tap.context.db.select(statement2, tap.context.db.$target.log));
  t.matchSnapshot(tap.context.db.select(statement3, tap.context.db.$target.log));

  const results1 = await tap.context.db.select(statement1);
  const results2 = await tap.context.db.select(statement2);
  const results3 = await tap.context.db.select(statement3);

  t.matchSnapshot(results1);
  t.matchSnapshot(results2);
  t.matchSnapshot(results3);
});

tap.only('limit clones the previous statement', async t => {
  const statement1 = tap.context.db.regular_table;
  const statement2 = statement1.limit(4);
  const statement3 = statement2.limit(2);

  t.matchSnapshot(tap.context.db.select(statement1, tap.context.db.$target.log));
  t.matchSnapshot(tap.context.db.select(statement2, tap.context.db.$target.log));
  t.matchSnapshot(tap.context.db.select(statement3, tap.context.db.$target.log));

  const results1 = await tap.context.db.select(statement1);
  const results2 = await tap.context.db.select(statement2);
  const results3 = await tap.context.db.select(statement3);

  t.matchSnapshot(results1);
  t.matchSnapshot(results2);
  t.matchSnapshot(results3);
});

tap.only('group clones the previous statement', async t => {
  const statement1 = tap.context.db.regular_table.project([tap.context.db.expr('1').as('one')]);
  const statement2 = statement1.group(statement1.$bool);
  const statement3 = statement2.group(statement1.$id);

  t.matchSnapshot(tap.context.db.select(statement1, tap.context.db.$target.log));
  t.matchSnapshot(tap.context.db.select(statement2, tap.context.db.$target.log));
  t.matchSnapshot(tap.context.db.select(statement3, tap.context.db.$target.log));

  const results1 = await tap.context.db.select(statement1);
  const results2 = await tap.context.db.select(statement2);
  const results3 = await tap.context.db.select(statement3);

  t.matchSnapshot(results1);
  t.matchSnapshot(results2);
  t.matchSnapshot(results3);
});

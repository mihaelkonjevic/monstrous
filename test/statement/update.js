import tap from 'tap';
import {connect, teardown, clean_snapshot} from '../util.js';

tap.cleanSnapshot = clean_snapshot;
tap.before(connect(tap));
tap.teardown(teardown(tap));

tap.test('updates and returns a record', async t => {
  const statement = tap.context.db.regular_table.filter(1);

  const changes = {
    bool: true,
    num: 123.45,
    txt: 'text'
  };

  const text = await tap.context.db.update(statement, changes, tap.context.db.$target.log);
  const rows = await tap.context.db.update(statement, changes);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 1);
  t.equal(rows[0].id, 1);
  t.matchSnapshot(rows, 'regular_table matches results');
});

tap.test('updates and returns a record as an object', async t => {
  const statement = tap.context.db.regular_table.filter(1);

  const changes = {
    bool: true,
    num: 5.4321,
    txt: 'more'
  };

  const text = await tap.context.db.update(statement, changes, tap.context.db.$target.log);
  const row = await tap.context.db.update(statement, changes, tap.context.db.$target.one);

  t.matchSnapshot(text, 'matches query text');
  t.ok(row);
  t.equal(row.id, 1);
  t.matchSnapshot(row, 'regular_table matches results');
});

tap.test('updates and returns multiple records', async t => {
  const statement = tap.context.db.regular_table.filter({bool: true});

  const changes = {
    bool: false,
    num: 678.9,
    txt: 'two'
  };

  const text = await tap.context.db.update(statement, changes, tap.context.db.$target.log);
  const rows = await tap.context.db.update(statement, changes);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 3);
  t.matchSnapshot(rows, 'regular_table matches results');
});

tap.test('updates composite types and domains with pgp.as.format, exprs, or tuples', async t => {
  const statement = tap.context.db.has_composite.filter(1);

  const changes = {
    first: {
      rawType: true, // rawType must be set to escape properly!
      // placeholders are for pg-promise and aren't sent to Postgres!
      toPostgres: () => tap.context.db.pgp.as.format('($1, $2)', [-3, 'negative'])
    },
    second: tap.context.db.expr('($1, $2)', [3, 'positive']),
    third: tap.context.db.tuple(4, 'also positive')
  };

  const text = await tap.context.db.update(statement, changes, tap.context.db.$target.log);
  const rows = await tap.context.db.update(statement, changes);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 1);
  t.matchSnapshot(rows, 'has_composite matches results');
});

import tap from 'tap';
import {connect, teardown, clean_snapshot} from '../util.js';

tap.cleanSnapshot = clean_snapshot;
tap.before(connect(tap));
tap.teardown(teardown(tap));

tap.test('deletes and returns a record', async t => {
  const statement = tap.context.db.regular_table.filter(1);

  const text = await tap.context.db.delete(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.delete(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 1);
  t.equal(rows[0].id, 1);
  t.matchSnapshot(rows, 'regular_table matches results');
});

tap.test('deletes and returns a record as an object', async t => {
  const statement = tap.context.db.regular_table.filter(2);

  const text = await tap.context.db.delete(statement, tap.context.db.$target.log);
  const row = await tap.context.db.delete(statement, tap.context.db.$target.one);

  t.matchSnapshot(text, 'matches query text');
  t.ok(row);
  t.equal(row.id, 2);
  t.matchSnapshot(row, 'regular_table matches results');
});

tap.test('deletes and returns multiple records', async t => {
  const statement = tap.context.db.regular_table.filter({bool: true});

  const text = await tap.context.db.delete(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.delete(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 2);
  t.matchSnapshot(rows, 'regular_table matches results');
});

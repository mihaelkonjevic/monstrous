import tap from 'tap';
import {connect, teardown, clean_snapshot} from '../util.js';

tap.cleanSnapshot = clean_snapshot;
tap.before(connect(tap));
tap.teardown(teardown(tap));

tap.test('unqualified string order', async t => {
  const table = tap.context.db.regular_table;
  const statement = table.order('id desc');
  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 5);
  t.matchSnapshot(rows, 'matches query result');
});

tap.test('column order', async t => {
  const table = tap.context.db.regular_table;
  const statement = table.order(`${table.$id} desc`);
  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 5);
  t.matchSnapshot(rows, 'matches query result');
});

tap.test('function(column) order', async t => {
  const table = tap.context.db.regular_table;
  const statement = table.order(tap.context.db.$order.desc(table.$id));
  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 5);
  t.matchSnapshot(rows, 'matches query result');
});

tap.test('expr order', async t => {
  const table = tap.context.db.regular_table;
  const statement = table.order(tap.context.db.$order.desc(tap.context.db.expr('id')));
  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 5);
  t.matchSnapshot(rows, 'matches query result');
});

tap.test('sort on multiple columns', async t => {
  const table = tap.context.db.regular_table;
  const statement = table
    .order(
      tap.context.db.$order.desc(table.$bool),
      tap.context.db.$order.asc(table.$num)
    );
  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 5);
  t.matchSnapshot(rows, 'matches query result');
});

tap.test('order direction asc is implicit', async t => {
  const table = tap.context.db.regular_table;
  const statement = table.order(table.$id);
  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 5);
  t.matchSnapshot(rows, 'matches query result');
});

drop schema public cascade;

create schema public;

create table regular_table (
  id int primary key generated always as identity,
  bool boolean,
  num float,
  txt text,
  inserted_at timestamptz not null default now()
);

insert into regular_table (bool, num, txt) values
  (true, 1.0, 'one'),
  (false, 2.1, 'two'),
  (true, 3.2, 'three'),
  (false, 4.3, 'four'),
  (true, 5.4, 'five');

create table json_table (
  id int primary key generated always as identity,
  js jsonb,
  inserted_at timestamptz not null default now()
);

insert into json_table (js) values
  ('{"alpha": 1, "beta": {"gamma": 2}}'::jsonb),
  ('[{"delta": 3}, {"epsilon": {"zeta": 4}}]'::jsonb);

create table join_parent (
  id int primary key generated always as identity,
  txt text
);

create table join_child (
  id int primary key generated always as identity,
  parent_id int references join_parent (id),
  txt text
);

create table join_multichild(
  id int primary key generated always as identity,
  parent_1_id int references join_parent (id),
  parent_2_id int references join_parent (id),
  txt text
);

create table join_ettin (
  id int primary key generated always as identity,
  ettin_id int references join_ettin (id),
  txt text
);

create table join_junction (
  parent_id int not null references join_parent (id),
  ettin_id int not null references join_ettin (id),
  primary key (parent_id, ettin_id)
);

insert into join_parent (txt) values ('one'), ('two'), ('three');
insert into join_child (parent_id, txt) values
  (1, 'one-one'),
  (2, 'two-one'),
  (2, 'two-two');

insert into join_ettin (ettin_id, txt) values
  (null, 'eerht'),
  (1, 'owt'),
  (1, 'eno');

insert into join_junction (parent_id, ettin_id) values
  (3, 3),
  (2, 2),
  (1, 1);

create type composite as (
  num int,
  tag text
);

create domain posite as composite
check ((value is null) or (value).num >= 0);

create table has_composite (
  id int primary key generated always as identity,
  first composite,
  second posite,
  third posite
);

insert into has_composite (first, second) values
  ((-1, 'negative'), (1, 'positive')),
  ((-2, 'negative'), (2, 'positive'));

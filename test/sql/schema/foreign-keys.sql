drop schema if exists foreign_keys cascade;
drop schema if exists foreign_keys_too cascade;

create schema foreign_keys;
create schema foreign_keys_too;

-- for convenience; search_path must be reset after or it will pollute
-- connections!
set search_path to foreign_keys, public;

create table alpha (
  id serial not null primary key,
  val text
);

create table beta (
  id serial not null primary key,
  alpha_id int,
  val text,
  val2 text,
  j jsonb,
  foreign key (alpha_id) references alpha(id)
);

alter table beta drop column val2;

create table gamma (
  id serial not null primary key,
  alpha_id_one int not null,
  alpha_id_two int,
  beta_id int,
  val text,
  j jsonb,
  foreign key (alpha_id_one) references alpha(id),
  foreign key (alpha_id_two) references alpha(id),
  foreign key (beta_id) references beta(id)
);

create table zeta (
  id serial not null primary key,
  val text
);

create table alpha_zeta (
  alpha_id int not null,
  zeta_id int not null
);

insert into alpha (val)
values ('one'), ('two'), ('three'), ('four');

insert into beta (alpha_id, val, j)
values
  (1, 'alpha one', null),
  (2, 'alpha two', null),
  (3, 'alpha three', null),
  (3, 'alpha three again', null),
  (null, 'not four', null),
  (null, 'not five', '{"x": {"y": "test"}}'::jsonb);

insert into gamma (alpha_id_one, alpha_id_two, beta_id, val, j)
values
  (1, 1, 1, 'alpha one alpha one beta one', null),
  (1, 2, 2, 'alpha two alpha two beta two', null),
  (2, 3, 2, 'alpha two alpha three beta two again', null),
  (2, null, 3, 'alpha two (alpha null) beta three', null),
  (3, 1, 4, 'alpha three alpha one beta four', null),
  (4, null, 5, 'alpha four beta five', '{"z": {"a": "test"}}'::jsonb),
  (4, 4, null, 'alpha four twice beta null', null);

insert into zeta (val)
values ('alpha one'), ('alpha one again'), ('alpha two');

insert into alpha_zeta (alpha_id, zeta_id)
values (1, 1), (1, 2), (2, 3);

create view beta_view as select * from beta;

create table foreign_keys_too.delta (
  id serial not null primary key,
  beta_id int,
  val text,
  foreign key (beta_id) references beta(id)
);

create table foreign_keys_too.epsilon (
  id serial not null primary key,
  alpha_id int,
  val text,
  foreign key (alpha_id) references alpha(id)
);

insert into foreign_keys_too.delta (beta_id, val)
values
  (1, 'beta one'),
  (2, 'beta two');

insert into foreign_keys_too.epsilon (alpha_id, val)
values
  (1, 'alpha one'),
  (null, 'not two');

create table selfjoin (
  id serial not null primary key,
  beta_id int references beta (id),
  parent_id int references selfjoin(id),
  val text not null
);

insert into selfjoin (beta_id, parent_id, val) values
  (1, null, 'one'),
  (2, 1, 'two'),
  (null, 1, 'three'),
  (1, null, 'four');

create table uuid_parent (
  id uuid not null default gen_random_uuid() primary key,
  val text,
  uu uuid
);

create table uuid_child (
  id uuid not null default gen_random_uuid() primary key,
  parent_id uuid not null references uuid_parent(id),
  val text,
  uu uuid
);

set search_path to public;

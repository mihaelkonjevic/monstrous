drop schema if exists documents cascade;
create schema documents;

-- for convenience; search_path must be reset after or it will pollute
-- connections!
set search_path to documents, public;
set search_path to public;

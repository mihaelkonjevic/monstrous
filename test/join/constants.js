import tap from 'tap';
import {connect, teardown, clean_snapshot} from '../util.js';

tap.cleanSnapshot = clean_snapshot;
tap.before(connect(tap, 'foreign-keys'));
tap.teardown(teardown(tap));

tap.test('joins on constants', async t => {
  const beta = tap.context.db.foreign_keys.beta;
  const epsilon = tap.context.db.foreign_keys_too.epsilon;
  const statement = beta
    .join(epsilon, {[epsilon.$val]: 'alpha one'})
    .filter({
      [beta.$val]: 'alpha three again'
    });

  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 1);
  t.matchSnapshot(rows, 'matches results');
});

tap.test('joins on multiple constants', async t => {
  const beta = tap.context.db.foreign_keys.beta;
  const epsilon = tap.context.db.foreign_keys_too.epsilon;
  const statement = beta
    .join(epsilon, {
      [epsilon.$id]: 1,
      [epsilon.$val]: 'alpha one'
    })
    .filter({
      [beta.$val]: 'alpha three again'
    });

  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 1);
  t.matchSnapshot(rows, 'matches results');
});

tap.test('joins on constants for multiple relations', async t => {
  const alpha = tap.context.db.foreign_keys.alpha;
  const beta = tap.context.db.foreign_keys.beta;
  const epsilon = tap.context.db.foreign_keys_too.epsilon;
  const statement = beta
    .join(alpha, {[alpha.$val]: 'one'})
    .join(epsilon, {
      [epsilon.$id]: 1,
      [epsilon.$val]: 'alpha one'
    })
    .filter({
      [beta.$val]: 'alpha three again'
    });

  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 1);
  t.matchSnapshot(rows, 'matches results');
});

tap.test('mixes keys and constants', async t => {
  const alpha = tap.context.db.foreign_keys.alpha;
  const epsilon = tap.context.db.foreign_keys_too.epsilon;
  const statement = alpha
    .join(epsilon, {
      [epsilon.$alpha_id]: alpha.$id,
      [epsilon.$val]: 'alpha one'
    });

  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 1);
  t.matchSnapshot(rows, 'matches results');
});

tap.test('handles simple operations', async t => {
  const alpha = tap.context.db.foreign_keys.alpha;
  const epsilon = tap.context.db.foreign_keys_too.epsilon;
  const statement = alpha
    .join(epsilon, {
      [`${epsilon.$alpha_id} is`]: null
    })
    .filter({
      [alpha.$val]: 'one'
    });

  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 1);
  t.matchSnapshot(rows, 'matches results');
});

tap.skip('handles operations with mutators', async t => {
  return db.alpha.join({
    'sch.epsilon': {
      type: 'INNER',
      on: {
        'alpha_id': [1, 2]
      }
    }
  }).find({val: 'one'}).then(result => {
    assert.deepEqual(result, [{
      id: 1,
      val: 'one',
      epsilon: [{
        id: 1,
        alpha_id: 1,
        val: 'alpha one'
      }]
    }]);
  });
});

tap.test('catches constants that start with valid keys', async t => {
  const alpha = tap.context.db.foreign_keys.alpha;
  const epsilon = tap.context.db.foreign_keys_too.epsilon;
  const statement = alpha
    .join(epsilon, {
      [epsilon.$alpha_id]: alpha.$id,
      [`${epsilon.$val} <>`]: `${alpha.$id} but literally the text ${alpha.$id}`
    });

  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 1);
  t.matchSnapshot(rows, 'matches results');
});

tap.skip('does json', async t => {
  const beta = tap.context.db.foreign_keys.beta;
  const gamma = tap.context.db.foreign_keys.gamma;
  const statement = beta
    .join(gamma, {
      [gamma.$j.z.a]: beta.$j.x.y,
    })
    .filter({
      [beta.$val]: 'not five'
    });

  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 1);
  t.matchSnapshot(rows, 'matches results');

//   return db.beta.join({
//     gamma: {
//       type: 'INNER',
//       on: {
//         'j.z.a': 'j.x.y'
//       }
//     }
//   }).find({val: 'not five'}).then(result => {
//     assert.deepEqual(result, [{
//       id: 6,
//       alpha_id: null,
//       val: 'not five',
//       j: {x: {y: 'test'}},
//       gamma: [{
//         id: 6, alpha_id_one: 4, alpha_id_two: null, beta_id: 5,
//         val: 'beta five', j: {z: {a: 'test'}}
//       }]
//     }]);
//   });
});

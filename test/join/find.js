import tap from 'tap';
import {connect, teardown, clean_snapshot} from '../util.js';

tap.cleanSnapshot = clean_snapshot;
tap.before(connect(tap, 'foreign-keys'));
tap.teardown(teardown(tap));

tap.test('returns a linear subtree from db.$target.one', async t => {
  const alpha = tap.context.db.foreign_keys.alpha;
  const beta = tap.context.db.foreign_keys.beta;
  const statement = alpha.join(beta)
    .filter({
      [alpha.$id]: 3
    })
    .limit(1);

  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const result = await tap.context.db.select(statement, tap.context.db.$target.one);

  t.matchSnapshot(text, 'matches query text');
  t.type(result, 'object')
  t.matchSnapshot(result, 'matches results');
});

tap.test('returns a linear subtree from db.$target.one with a primitive pk', async t => {
  const alpha = tap.context.db.foreign_keys.alpha;
  const beta = tap.context.db.foreign_keys.beta;
  const statement = alpha.join(beta).filter(3).limit(1);

  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const result = await tap.context.db.select(statement, tap.context.db.$target.one);

  t.matchSnapshot(text, 'matches query text');
  t.type(result, 'object')
  t.matchSnapshot(result, 'matches results');
});

tap.test('returns an array from find with a primitive pk', async t => {
  const alpha = tap.context.db.foreign_keys.alpha;
  const beta = tap.context.db.foreign_keys.beta;
  const statement = alpha.join(beta).filter(3);

  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const result = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.type(result, 'object')
  t.matchSnapshot(result, 'matches results');
});

import tap from 'tap';
import {connect, teardown, clean_snapshot} from '../util.js';

tap.cleanSnapshot = clean_snapshot;
tap.before(connect(tap, 'foreign-keys'));
tap.teardown(teardown(tap));

tap.test('defers to an explicit relation but aliases to the key', async t => {
  const statement = tap.context.db.foreign_keys.alpha
    .join(tap.context.db.foreign_keys.beta.as('asdf'))
    .filter({
      [tap.context.db.foreign_keys.alpha.$id]: 3
    });

  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 1);
  t.matchSnapshot(rows, 'joins parent and child tables, with child aliased');
});

tap.test('aliases to the table name when processing an implicit relation in another schema', async t => {
  const statement = tap.context.db.foreign_keys.alpha
    .join(tap.context.db.foreign_keys_too.epsilon);

  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 1);
  t.matchSnapshot(rows, 'matches results');
});

tap.test('joins the same table multiple times under different aliases', async t => {
  const alpha = tap.context.db.foreign_keys.alpha;
  const gamma = tap.context.db.foreign_keys.gamma;
  const statement = gamma
    .join(alpha.as('alpha1'), {[alpha.as('alpha1').$id]: gamma.$alpha_id_one})
    .join(alpha.as('alpha2'), {[alpha.as('alpha2').$id]: gamma.$alpha_id_two})
    .filter({
      [alpha.as('alpha1').$id]: 3
    });

  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 1);
  t.matchSnapshot(rows, 'matches results');
});

tap.test('allows self joins with an alias', async t => {
  const alpha = tap.context.db.foreign_keys.alpha;
  const alpha_again = alpha.as('alpha_again');
  const statement = alpha
    .join(alpha_again, {[alpha.$id]: alpha_again.$id})
    .filter({[alpha.$id]: 1});

  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 1);
  t.matchSnapshot(rows, 'matches results');
});

tap.test('errors if an unknown unaliased form is referenced in join criteria', async t => {
  const alpha = tap.context.db.foreign_keys.alpha;
  const gamma = tap.context.db.foreign_keys.gamma;
  const statement = gamma
    .join(alpha.as('alpha1'), {[alpha.as('alpha1').$id]: gamma.$alpha_id_one})
    .join(alpha.as('alpha2'), {[alpha.$id]: gamma.$alpha_id_two}) // missing in the `on`
    .filter({
      [alpha.as('alpha1').$id]: 3
    });

  return t.rejects(
    () => tap.context.db.select(statement).catch(err => {
      t.equal(err.code, '42P01', 'errcode correct');
      t.equal(err.message, 'invalid reference to FROM-clause entry for table "alpha"', 'error message correct');

      throw err;
    })
  );
});

tap.test('errors if an unknown unaliased form is referenced in where criteria', async t => {
  const alpha = tap.context.db.foreign_keys.alpha;
  const gamma = tap.context.db.foreign_keys.gamma;
  const statement = gamma
    .join(alpha.as('alpha1'), {[alpha.as('alpha1').$id]: gamma.$alpha_id_one})
    .join(alpha.as('alpha2'), {[alpha.as('alpha2').$id]: gamma.$alpha_id_two})
    .filter({
      [alpha.$id]: 3 // missing in the `where`
    });

  return t.rejects(
    () => tap.context.db.select(statement).catch(err => {
      t.equal(err.code, '42P01', 'errcode correct');
      t.equal(err.message, 'invalid reference to FROM-clause entry for table "alpha"', 'error message correct');

      throw err;
    })
  );
});

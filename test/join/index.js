import tap from 'tap';
import {connect, teardown, clean_snapshot} from '../util.js';

tap.cleanSnapshot = clean_snapshot;
tap.before(connect(tap, 'foreign-keys'));
tap.teardown(teardown(tap));

tap.test('basic join', async t => {
  const statement = tap.context.db.join_parent.join(tap.context.db.join_child, {
    [tap.context.db.join_child.$parent_id]: tap.context.db.join_parent.$id
  });
  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 2);
  t.matchSnapshot(rows, 'joins parent and child tables');
});

tap.test('basic join, unqualified key', async t => {
  const statement = tap.context.db.join_parent.join(tap.context.db.join_child, {
    parent_id: tap.context.db.join_parent.$id
  });
  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 2);
  t.matchSnapshot(rows, 'joins parent and child tables');
});

tap.test('basic join, on parent to child', async t => {
  const statement = tap.context.db.join_parent.join(tap.context.db.join_child, {
    [tap.context.db.join_parent.$id]: tap.context.db.join_child.$parent_id
  });
  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 2);
  t.matchSnapshot(rows, 'joins parent and child tables');
});

tap.test('basic join, on child to parent', async t => {
  const statement = tap.context.db.join_parent.join(tap.context.db.join_child, {
    [tap.context.db.join_child.$parent_id]: tap.context.db.join_parent.$id
  });
  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 2);
  t.matchSnapshot(rows, 'joins parent and child tables');
});

tap.test('basic join, returning nothing', async t => {
  const statement = tap.context.db.join_parent.join(tap.context.db.join_child, {
    parent_id: tap.context.db.join_parent.$id
  }).filter({
    [tap.context.db.join_parent.$txt]: 'nope'
  });
  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 0);
  t.matchSnapshot(rows, 'joins parent and child tables');
});

tap.test('using foreign keys from the parent', async t => {
  const statement = tap.context.db.join_parent.join(tap.context.db.join_child);
  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 2);
  t.matchSnapshot(rows, 'joins parent and child tables');
});

tap.test('using foreign keys from the referent', async t => {
  const statement = tap.context.db.join_child.join(tap.context.db.join_parent);
  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 3);
  t.matchSnapshot(rows, 'joins child and parent tables');
});

tap.test('foreign keys from parent, referent aliased', async t => {
  const statement = tap.context.db.join_parent.join(tap.context.db.join_child.as('children'));
  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 2);
  t.matchSnapshot(rows, 'joins parent and child tables, with child aliased');
});

tap.test('foreign keys from referent, parent aliased', async t => {
  const statement = tap.context.db.join_child.join(tap.context.db.join_parent.as('par'));
  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 3);
  t.matchSnapshot(rows, 'joins child and parent tables, with parent aliased');
});

tap.test('throws if no candidate foreign key', async t => {
  t.throws(
    () => tap.context.db.join_parent.join(tap.context.db.join_ettin),
    new Error('no join condition and no available foreign key relationship for join_ettin')
  );
});

tap.test('throws if more than one candidate foreign key', async t => {
  t.throws(
    () => tap.context.db.join_parent.join(tap.context.db.join_multichild),
    new Error('multiple possible foreign keys for join_multichild, specify an explicit join condition')
  );
});

tap.test('with parameters in join condition, all columns qualified', async t => {
  const statement = tap.context.db.join_parent.join(tap.context.db.join_child, {
    [tap.context.db.join_child.$parent_id]: tap.context.db.join_parent.$id,
    [tap.context.db.join_child.$txt]: 'two-one'
  });
  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 1);
  t.matchSnapshot(rows, 'joins parent and child tables');
});

tap.test('with parameters in join condition, ambiguous column qualified', async t => {
  const statement = tap.context.db.join_parent.join(tap.context.db.join_child, {
    parent_id: tap.context.db.join_parent.$id,
    [tap.context.db.join_child.$txt]: 'two-one'
  });
  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 1);
  t.matchSnapshot(rows, 'joins parent and child tables');
});

tap.test('with parameters in join condition, relying on auto-association to child', async t => {
  const statement = tap.context.db.join_parent.join(tap.context.db.join_child, {
    parent_id: tap.context.db.join_parent.$id,
    [tap.context.db.join_child.$txt]: 'two-one'
  });
  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 1);
  t.matchSnapshot(rows, 'joins parent and child tables');
});

tap.test('many:many join', async t => {
  const statement = tap.context.db.join_parent
    .join(tap.context.db.join_junction)
    .join(tap.context.db.join_ettin);
  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 3);
  t.matchSnapshot(rows, 'joins parent and child tables');
});

tap.test('joins a relation in another schema', async t => {
  const alpha = tap.context.db.foreign_keys.alpha;
  const epsilon = tap.context.db.foreign_keys_too.epsilon;
  const statement = alpha.join(epsilon, {[epsilon.$alpha_id]: alpha.$id});

  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 1);
  t.matchSnapshot(rows, 'matches results');
});

tap.test('joins a relation in another schema through an implicit foreign key relationship', async t => {
  const alpha = tap.context.db.foreign_keys.alpha;
  const statement = alpha.join(tap.context.db.foreign_keys_too.epsilon);

  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 1);
  t.matchSnapshot(rows, 'matches results');
});

tap.test('can join on any fields', async t => {
  const beta = tap.context.db.foreign_keys.beta;
  const epsilon = tap.context.db.foreign_keys_too.epsilon;
  const statement = beta
    .join(epsilon, {[beta.$val]: epsilon.$val});

  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 1);
  t.matchSnapshot(rows, 'matches results');
});

tap.test('can join with operation', async t => {
  const alpha = tap.context.db.foreign_keys.alpha;
  const beta = tap.context.db.foreign_keys.beta;
  const statement = alpha
    .join(beta, {[`${beta.$alpha_id} <>`]: alpha.$id});

  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 4);
  t.matchSnapshot(rows, 'matches results');
});

tap.test('changes join types', async t => {
  const alpha = tap.context.db.foreign_keys.alpha;
  const beta = tap.context.db.foreign_keys.beta;
  const statement = alpha
    .join(beta, tap.context.db.$join.left, {[beta.$alpha_id]: alpha.$id})
    .filter({
      [`${alpha.$id} >`]: 1
    });

  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 3);
  t.matchSnapshot(rows, 'matches results');
});

tap.test('joins on TRUE', async t => {
  const statement = tap.context.db.foreign_keys.alpha
    .join(tap.context.db.foreign_keys_too.epsilon, true)
    .filter(1);

  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const rows = await tap.context.db.select(statement);

  t.matchSnapshot(text, 'matches query text');
  t.equal(rows.length, 1);
  t.matchSnapshot(rows, 'matches results');
});

tap.test('errors when the origin name reappears', async t => {
  const alpha = tap.context.db.foreign_keys.alpha;
  t.throws(
    () => alpha.join(alpha, {[alpha.$id]: 3}),
    new Error('bad join definition: alpha is repeated')
  );
});

tap.test('errors when another relation name reappears', async t => {
  const alpha = tap.context.db.foreign_keys.alpha;
  const beta = tap.context.db.foreign_keys.beta;
  t.throws(() => beta
      .join(alpha)
      .join(alpha, {[alpha.$id]: 3}),
    new Error('bad join definition: alpha is repeated')
  );
});

tap.test('errors if relation is not known', async t => {
  return t.throws(
    () => tap.context.db.foreign_keys.alpha.join(tap.context.db.not_a_real_table),
    new Error('bad join definition: not a relation, statement, or values list')
  );
});

import tap from 'tap';
import {connect, teardown, clean_snapshot} from '../util.js';

tap.cleanSnapshot = clean_snapshot;
tap.before(connect(tap));
tap.teardown(teardown(tap));

tap.test('runs tasks on a single connection', async t => {
  const counter = tap.context.db.regular_table.project({
    count: tap.context.db.expr('count(*)')
  });
  let count = await tap.context.db.select(counter, tap.context.db.$target.unit);

  t.same(count, 5);

  await t.resolves(
    async () => {
      await tap.context.db.task(async task => {
        const records = await task.insert(tap.context.db.regular_table, {txt: 'abc'});

        t.ok(records);
        t.same(records[0].id, 6);

        count = await tap.context.db.select(counter, tap.context.db.$target.unit);

        t.same(count, 6); // tasks do not execute in a transactional context!

        // note query targets must be specified on task not the original db
        count = await task.select(counter, task.$target.unit);

        t.same(count, 6);

        const added = await task.update(tap.context.db.regular_table.filter(6), {txt: 'def'});

        return t.matchSnapshot(added);
      })
    }
  );

  count = await tap.context.db.select(counter, tap.context.db.$target.unit);

  t.same(count, 6);
});

tap.test('commits transactions', async t => {
  const counter = tap.context.db.regular_table.project({
    count: tap.context.db.expr('count(*)')
  });
  let count = await tap.context.db.select(counter, tap.context.db.$target.unit);

  t.same(count, 6);

  await t.resolves(
    async () => {
      await tap.context.db.transaction(async tx => {
        const records = await tx.insert(tap.context.db.regular_table, {txt: 'ghi'});

        t.ok(records);
        t.same(records[0].id, 7);

        count = await tap.context.db.select(counter, tap.context.db.$target.unit);

        t.same(count, 6); // outside the transaction!

        // note query targets must be specified on tx not the original db
        count = await tx.select(counter, tx.$target.unit);

        t.same(count, 7);

        const added = await tx.update(tap.context.db.regular_table.filter(7), {txt: 'jkl'});

        return t.matchSnapshot(added);
      })
    }
  );

  count = await tap.context.db.select(counter, tap.context.db.$target.unit);

  t.same(count, 7);
});

tap.test('rolls transactions back on error', async t => {
  const counter = tap.context.db.regular_table.project({
    count: tap.context.db.expr('count(*)')
  });
  let count = await tap.context.db.select(counter, tap.context.db.$target.unit);

  t.same(count, 7);

  await t.rejects(
    async () => {
      await tap.context.db.transaction(async tx => {
        const records = await tx.insert(tap.context.db.regular_table, {txt: 'mno'});

        t.ok(records);
        t.same(records[0].id, 8);

        count = await tap.context.db.select(counter, tap.context.db.$target.unit);

        t.same(count, 7); // outside the transaction!

        // note query targets must be specified on tx not the original db
        count = await tx.select(counter, tx.$target.unit);

        t.same(count, 8);

        await tx.update(tap.context.db.regular_table.filter(8), {txt: 'pqr'});

        throw new Error('end of test')
      })
    },
    new Error('end of test')
  );

  count = await tap.context.db.select(counter, tap.context.db.$target.unit);

  t.same(count, 7);
});

tap.test('saves: inserts', async t => {
  const statement = tap.context.db.regular_table;
  const text = await tap.context.db.save(statement, {
    txt: 'new'
  }, tap.context.db.$target.log);
  const result = await tap.context.db.save(statement, {txt: 'new'});

  t.matchSnapshot(text, 'matches query text');
  t.equal(result.id, 9);
  t.matchSnapshot(result, 'inserts new item');
});

tap.test('saves: updates', async t => {
  const statement = tap.context.db.regular_table;
  const text = await tap.context.db.save(statement, {
    id: 1,
    txt: 'saved'
  }, tap.context.db.$target.log);
  const result = await tap.context.db.save(statement, {id: 1, txt: 'saved'});

  t.matchSnapshot(text, 'matches query text');
  t.equal(result.txt, 'saved');
  t.matchSnapshot(result, 'updates existing item');
});

tap.test('saves: errors if statement provided', async t => {
  t.throws(
    () => tap.context.db.save(tap.context.db.regular_table.limit(1), {txt: 'saved'}),
    new Error('save may only be invoked for relations, not statements')
  );
});

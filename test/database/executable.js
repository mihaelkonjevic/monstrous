import tap from 'tap';
import {connect, teardown, clean_snapshot} from '../util.js';

tap.cleanSnapshot = clean_snapshot;
tap.before(connect(tap));
tap.teardown(teardown(tap));

tap.test('run a queryfile', async t => {
  const result = await tap.context.db.execute(tap.context.db.many);

  t.matchSnapshot(result, 'matches results');
});

tap.test('run a queryfile with named parameters', async t => {
  const result = await tap.context.db.execute(tap.context.db.named, {
    first: 1,
    second: 2
  });

  t.matchSnapshot(result, 'matches results');
});

tap.test('supply the `one` target to a queryfile', async t => {
  const result = await tap.context.db.execute(
    tap.context.db.one,
    'text',
    tap.context.db.$target.one
  );

  t.matchSnapshot(result, 'matches results');
});

tap.test('supply the `unit` target to a queryfile', async t => {
  const result = await tap.context.db.execute(
    tap.context.db.one,
    'text',
    tap.context.db.$target.unit
  );

  t.matchSnapshot(result, 'matches results');
});

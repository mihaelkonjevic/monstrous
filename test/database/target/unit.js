import tap from 'tap';
import {connect, teardown, clean_snapshot} from '../../util.js';

tap.cleanSnapshot = clean_snapshot;
tap.before(connect(tap));
tap.teardown(teardown(tap));

tap.test('returns a single value for db.$target.unit', async t => {
  const regular_table = tap.context.db.regular_table;
  const statement = regular_table.filter(3)
    .project([regular_table.$txt])
    .limit(1);

  const text = await tap.context.db.select(statement, tap.context.db.$target.log);
  const value = await tap.context.db.select(statement, tap.context.db.$target.unit);

  t.matchSnapshot(text, 'matches query text');
  t.equal(value, 'three');
});

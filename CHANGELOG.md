# Changelog

All notable changes to this project will be documented in this file. See [commit-and-tag-version](https://github.com/absolute-version/commit-and-tag-version) for commit guidelines.

## [0.1.2](https://gitlab.com/monstrous/monstrous/compare/v0.1.1...v0.1.2) (2023-02-19)


### Bug Fixes

* projection clones set #to_array ([4409912](https://gitlab.com/monstrous/monstrous/commit/4409912f37fcd557bb0268297a0ed6d7d94801d6))

## [0.1.1](https://gitlab.com/monstrous/monstrous/compare/v0.1.0...v0.1.1) (2023-02-17)


### Features

* composite types with Tuple ([cdc881f](https://gitlab.com/monstrous/monstrous/commit/cdc881fbf2f2ec8203b9c837057a5a5f3be27a50))

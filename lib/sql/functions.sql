select distinct -- ignore overloads (quick version)
  case
    when n.nspname = current_schema then null
    else n.nspname
  end as "schema",
  p.proname as "name",
  p.provariadic as "is_variadic",
  (p.prokind = 'p') as "is_procedure",
  (not p.proretset) as "is_single_row",
  (t.typtype in ('b', 'd', 'e', 'r')) as "is_single_value"
from pg_proc as p
join pg_namespace as n on p.pronamespace = n.oid
join pg_type as t on p.prorettype = t.oid
where n.nspname not in ('pg_catalog', 'information_schema');

with classes as (
  select
    c.oid,
    c.relname,
    n.nspname,
    (
      (c.relkind = any (array['r', 'p'])) -- tables (incl. partitioned) are always updatable
      or (c.relkind = any (array['v', 'f'])) -- views and foreign tables need an extra check
      and (pg_relation_is_updatable(c.oid::regclass, false) & 8) = 8
    ) as is_updatable
  from pg_catalog.pg_class as c
  inner join pg_catalog.pg_namespace as n on n.oid = c.relnamespace
  where n.nspname not in ('information_schema', 'pg_catalog', 'pg_toast')
    and c.relkind in ('r', 'p', 'v', 'f')
), attributes as (
  select
    a.attrelid,
    json_agg(json_build_object(
      'name', a.attname::text,
      'type', t.typname::text,
      'category', t.typcategory::text
    ) order by a.attnum) as columns
  from pg_catalog.pg_attribute as a
  inner join pg_catalog.pg_type as t on t.oid = a.atttypid
  where a.attnum > 0 and a.attisdropped is false
  group by a.attrelid
), primary_keys as (
  select c.conrelid, array_agg(a.attname::text order by a.attnum) as columns
  from pg_catalog.pg_constraint as c
  inner join pg_catalog.pg_attribute as a
    on a.attrelid = c.conrelid
    and a.attnum = any(c.conkey)
  where c.contype = 'p'
  group by c.conrelid
), foreign_keys as (
  select
    con.conname,
    orel.oid as origin,
    array_agg(oatt.attname::text order by oatt.attnum) as origin_columns,
    rrel.oid as reference,
    array_agg(ratt.attname::text order by ratt.attnum) as reference_columns
  from pg_catalog.pg_constraint as con
  inner join pg_catalog.pg_class as orel on orel.oid = con.confrelid
  inner join pg_catalog.pg_attribute as oatt
    on oatt.attrelid = orel.oid
    and oatt.attnum = any(con.confkey)
  inner join pg_catalog.pg_class as rrel on rrel.oid = con.conrelid
  inner join pg_catalog.pg_attribute as ratt
    on ratt.attrelid = rrel.oid
    and ratt.attnum = any(con.conkey)
  where con.contype = 'f'
  group by con.conname, orel.oid, rrel.oid
), class_fks as (
  select
    fks.reference,
    json_agg(json_build_object(
      'origin_schema', case when c.nspname = current_schema then null else c.nspname end,
      'origin', c.relname,
      'origin_columns', fks.origin_columns,
      'reference_schema', case when cr.nspname = current_schema then null else cr.nspname end,
      'reference', cr.relname,
      'reference_columns', fks.reference_columns
    )) as foreign_keys
  from foreign_keys as fks
  join classes as c on c.oid = fks.origin
  join classes as cr on cr.oid = fks.reference
  group by reference
), inheritance as (
  select
    inh.inhrelid,
    n.nspname as parent_schema,
    c.relname as parent_name
  from pg_catalog.pg_inherits as inh
  join pg_catalog.pg_class as c on c.oid = inh.inhparent
  join pg_catalog.pg_namespace as n on n.oid = c.relnamespace
)
select
  case
    when c.nspname = current_schema then null
    else c.nspname
  end as "schema",
  c.relname as "name",
  c.is_updatable,
  inh.parent_schema,
  inh.parent_name,
  tc.columns,
  pk.columns as primary_key,
  fks.foreign_keys
from classes as c
inner join attributes as tc on tc.attrelid = c.oid
left outer join primary_keys as pk on pk.conrelid = c.oid
left outer join class_fks as fks on fks.reference = c.oid
left outer join inheritance as inh on inh.inhrelid = c.oid;

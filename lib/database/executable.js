import pgp from 'pg-promise';

export default class Executable {
  #qrm = pgp.queryResult.any;

  constructor (spec) {
    if (spec instanceof pgp.QueryFile) {
      this.sql = spec;
    } else {
      this.schema = spec.schema;
      this.name = spec.name;

      // functions can be overloaded or variadic, so we build the sql text at
      // runtime with however many arguments we've been given
      if (spec.is_procedure) {
        this.#qrm = pgp.queryResult.none;
        this.sql = params => `call ${this.path}(${params})`;
      } else {
        this.sql = params => `select * from ${this.path}(${params})`;

        if (spec.is_single_row) {
          this.#qrm = pgp.queryResult.one;
        }

        // TODO single value/unit
      }
    }
  }

  get path() {
    if (this.schema) {
      return `"${this.schema}"."${this.name}"`;
    }

    return `"${this.name}"`;
  }

  invoke() {
    const args = [...arguments];

    if (this.sql instanceof pgp.QueryFile) {
      return [this.sql, ...args, this.#qrm];
    }

    const params = args.map((_, idx) => `$${idx + 1}`);

    return [this.sql(params), args, this.#qrm];
  }
}

import pgp from 'pg-promise';

export default class Expr {
  alias;

  constructor (sql, params, options = {}) {
    this.sql = sql;
    this.params = params || [];
    this.options = options;
  }

  get [Symbol.toStringTag]() {
    return 'Expr';
  }

  as(alias) {
    this.alias = alias;

    return this;
  }

  compile() {
    return pgp.as.format(this.sql, this.params, this.options);
  }
}

export class Tuple extends Expr {
  constructor() {
    super();

    this.params = [...arguments];
  }

  get [Symbol.toStringTag]() {
    return 'Tuple';
  }

  compile(raw) {
    const placeholders = this.params.map((p, idx) => {
      if (p instanceof Expr) return `$${idx + 1}:raw`;

      return `$${idx + 1}`;
    }).join(', ');

    return {
      rawType: raw === true,
      toPostgres: () => pgp.as.format(
        `(${placeholders})`,
        this.params.map(p => {
          if (p instanceof Expr) {
            return p.compile(true); // unused in Expr, but also covers nested Tuples
          }

          return p;
        }),
        this.options
      )
    };
  }
}

/**
 * Tokenize a string representing a field in a database relation.
 * @param {String} str - the string to process.
 * @return {Array} A list of tokens.
 */
export default function lex(str) {
  str = str.trim();

  const tokens = [[]];        // we're going to collect token arrays
  let buffer = tokens[0];     // start with the first token
  let in_quotation = false;   // ensure we pick up everything in quotes
  let i = 0;
  let char = str.charAt(i);

  do {
    if (in_quotation && char === '"') {
      in_quotation = !in_quotation;

      buffer.push(char);
    } else {
      switch (char) {
        case '"':
          // quoted token
          if (in_quotation) {
            // closing a quotation completes the token
            buffer = tokens[tokens.push([]) - 1];
          }

          in_quotation = !in_quotation;

          buffer.push(char);

          break;

        case ' ': case '\t': case '\r': case '\n':
          // whitespace; separates tokens
          buffer = tokens[tokens.push([]) - 1];
          break;

        default:
          buffer.push(char);
          break;
      }
    }

    i++;
  } while (char = str.charAt(i));

  return tokens.reduce(function (acc, p) {
    const str = p.join('').trim();

    if (str) { acc.push(str); }

    return acc;
  }, []);
};

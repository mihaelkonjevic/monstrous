import pgp from 'pg-promise';
import {validate as uuid_validate} from 'uuid';

import unquote from '../util/unquote.js';
import Expr, {Tuple} from './expr.js';
import Join from './join.js';
import PredicateTree from './predicate-tree.js';
import Values from './values.js';
import {Alias} from '../database/relation.js';
import {insert_tree, insert_values} from './insert.js';
import Projection from './project.js';

export const $order = {
  asc: val => `${val instanceof Expr ? val.sql : val} asc`,
  desc: val => `${val instanceof Expr ? val.sql : val} desc`
};

export default class Statement {
  #join = [];
  #filter;
  #group = [];
  #order = [];
  #projection;
  #offset;
  #limit;

  name; // alias for subqueries or values lists

  constructor (relation) {
    this.#join.push(new Join(null, relation))
    this.#projection = new Projection(relation.primary_key, relation.qualified);

    this.name = relation.name;

    for (const q of relation.qualified || []) {
      const field = q.split('.').pop();

      // define getters since the name could be changed!
      Object.defineProperty(this, `$${unquote(field)}`, {
        get: function () {
          return `"${this.name}".${field}`;
        }
      });
    }
  }

  // TODO only used for projection.augment, can we reorganize that?
  get projection() {
    return this.#projection;
  }

  *[Symbol.iterator]() {
    for (const column of this.#projection.columns()) {
      if (this.name) {
        yield `"${this.name}"."${column}"`;
      }

      yield `"${column}"`;
    }
  }

  articulate(result) {
    return this.#projection.articulate(result);
  }

  clone(except_prop) {
    const next = Object.assign(new Statement(this.#join[0].relation), this);

    if (except_prop !== '#join') next.#join = [...this.#join];
    if (except_prop !== '#group') next.#group = [...this.#group];
    if (except_prop !== '#order') next.#order = [...this.#order];
    if (except_prop !== '#filter') next.#filter = this.#filter && this.#filter.clone();
    if (except_prop !== '#projection') next.#projection = this.#projection.clone();

    next.#offset = this.#offset;
    next.#limit = this.#limit;

    return next;
  }

  // builders

  only() {
    return this;
  }

  as(alias) {
    const next = this.clone();

    next.name = alias;

    return next;
  }

  join(joinable, ...args) {
    const next = this.clone('#join');

    const j = new Join(this.#join, joinable, ...args);

    next.#join = [...this.#join, j]; // previous joins can't change
    next.#projection.augment(j.path, j.relation);

    return next;
  }

  filter(...filters) {
    const next = this.clone('#filter');

    next.#filter = this.#filter ? this.#filter.clone() : new PredicateTree(
      next.#join.reduce(
        (acc, j) => typeof j.relation[Symbol.iterator] === 'function' ? acc.concat([...j.relation]) : acc,
        []
      ).flat()
    );

    if (
      filters.length === 1 &&
      next.#join[0].relation.primary_key &&
      next.#join[0].relation.primary_key.length === 1 && (
        (!isNaN(filters[0]) && Number.isInteger(filters[0])) ||
        (Object.prototype.toString.call(filters[0]) === '[object String]' && uuid_validate(filters[0]))
      )
    ) {
      next.#filter.add({
        [next.#join[0].relation.primary_key[0]]: filters[0]
      });
    } else {
      next.#filter.add(...filters);
    }

    return next;
  }

  distinct() {
    return this;
  }

  project(projection) {
    const next = this.clone('#projection');

    if (Array.isArray(projection)) {
      next.#projection = new Projection(this.#join[0].relation.primary_key, projection);
    } else if (Object.prototype.toString.call(projection) === '[object Object]') {
      next.#projection = Projection.from(projection);
    } else {
      throw new Error('projection must be an array of columns or an object representing a single output record');
    }

    return next;
  }

  window(partition_by, order_by) {
    return this;
  }

  group(...fields) {
    const next = this.clone('#group');

    Array.prototype.push.apply(next.#group, fields);

    return next;
  }

  order(...fields) {
    const next = this.clone();

    next.#order = fields;

    return next;
  }

  offset(num) {
    const next = this.clone();

    next.#offset = num;

    return next;
  }

  limit(num) {
    const next = this.clone();

    next.#limit = num;

    return next;
  }

  // formatters

  select() {
    const text = ['select'];
    const params = [];
    let param_idx = 1;

    text.push(this.#projection.flatten().join(', '));
    text.push(`from ${this.#join[0].relation.path}`);

    if (this.#join[0].relation instanceof Alias) {
      text.push(`as ${this.#join[0].relation.name}`)
    }

    for (let [idx, join] of this.#join.entries()) {
      if (idx === 0) continue

      const {
        sql: join_sql,
        params: join_params
      } = join.on.compile(param_idx);

      if (join.relation instanceof Statement) {
        const subquery = join.relation.select();

        text.push(`${join.type.description} (${subquery.sql}) as "${join.relation.name}" on ${join_sql}`);
        Array.prototype.push.apply(params, subquery.params);
        param_idx += subquery.params.length;
      } else if (join.relation instanceof Values) {
        text.push(`${join.type.description} (values`);

        const {
          sql: values_sql,
          params: values_params
        } = join.relation.compile(param_idx);

        text.push(`${values_sql}) as ${join.relation.name} (${join.relation.keys}) on ${join_sql}`);

        Array.prototype.push.apply(params, values_params);
        param_idx += values_params.length;
      } else if (join.relation instanceof Alias) {
        text.push(`${join.type.description} ${join.relation.path} as "${join.relation.name}" on ${join_sql}`);
      } else {
        text.push(`${join.type.description} ${join.relation.path} on ${join_sql}`);
      }

      Array.prototype.push.apply(params, join_params);

      param_idx += join_params.length;
    }

    if (this.#filter && this.#filter.is_active) {
      const {
        sql: filter_sql,
        params: filter_params
      } = this.#filter.compile(param_idx);

      text.push(`where ${filter_sql}`);

      Array.prototype.push.apply(params, filter_params);

      param_idx += filter_params.length;
    }

    if (this.#group.length > 0) {
      text.push('group by');

      text.push(this.#group.join(', '));
    }

    if (this.#order.length > 0) {
      text.push('order by');

      text.push(this.#order.join(', '));
    }

    if (this.#offset) {
      text.push(`offset ${this.#offset}`);
    }

    if (this.#limit) {
      text.push(`limit ${this.#limit}`);
    }

    return {
      sql: text.join(' '),
      params
    };
  }

  insert(...values) {
    if (values.length === 1 && Array.isArray(values[0])) {
      throw new Error('values to insert must objects; use the ... spread operator to pass an array');
    }

    if (this.#join.length === 1) {
      if (values.length === 1 && values[0] instanceof Statement) {
        const text = [];

        text.push(`insert into ${this.#join[0].relation.path}`);
        text.push(`(${values[0].#projection.columns().join(', ')})`);

        const {
          sql: subquery_sql,
          params
        } = values[0].select();

        text.push(subquery_sql);
        text.push('returning *');

        return {
          sql: text.join(' '),
          params
        };
      } else {
        return insert_values(this.#join[0].relation, values);
      }
    }

    if (values.length > 1) {
      throw new Error('multi-table inserts may only add multiple records to leaf tables');
    }

    const {
      sql: root_sql,
      params,
      param_idx
    } = insert_values(
      this.#join[0].relation,
      values,
      null, // TODO conflict
      Object.keys(values[0]).filter(k => this.#join[0].relation.has(k))
    );

    const {
      sql: cte_sql,
      params: cte_params
    } = insert_tree(this.#join[0].relation, this.#join, values, param_idx);

    return {
      // TODO restrict fields returned based on projection
      sql: `with ${this.#join[0].relation.name}_cte as (${root_sql}) ${cte_sql} select * from ${this.#join[0].relation.name}_cte`,
      params: params.concat(cte_params)
    };
  }

  update(changes) {
    const text = [`update ${this.#join[0].relation.path} set`];
    const params = [];
    let param_idx = 1;

    text.push(
      Object.entries(changes)
        .map(([attr, change]) => {
          if (this.#join.some(j => j.relation.qualified.indexOf(change) > -1)) {
            return `${attr} = ${change}`;
          }

          if (change instanceof Tuple) {
            params.push(change.compile(true));
          } else if (change instanceof Expr) {
            return `${attr} = ${change.compile()}`;
          } else {
            params.push(change);
          }

          return `${attr} = $${param_idx++}`;
        })
        .join(', ')
    );

    if (this.#join.length > 1) {
      text.push(`from ${this.#join[1].relation.path}`);

      for (let jidx = 2; jidx < this.#join.length; jidx++) {
        const join = this.#join[jidx];
        const {
          sql: join_sql,
          params: join_params
        } = join.on.compile(param_idx);

        if (join.relation instanceof Alias) {
          text.push(`${join.type.description} ${join.relation.path} as "${join.relation.name}" on ${join_sql}`);
        } else {
          text.push(`${join.type.description} ${join.relation.path} on ${join_sql}`);
        }
        Array.prototype.push.apply(params, join_params);

        param_idx += join_params.length;
      }

      const {
        sql: first_join_sql,
        params: first_join_params
      } = this.#join[1].on.compile(param_idx); // first non-origin relation!

      param_idx += first_join_params.length;
      Array.prototype.push.apply(params, first_join_params);

      text.push(`where ${first_join_sql}`);
    }

    if (this.#filter && this.#filter.is_active) {
      text.push(this.#join.length > 1 ? 'and' : 'where');

      const {
        sql: filter_sql,
        params: filter_params
      } = this.#filter.compile(param_idx);

      text.push(filter_sql);

      Array.prototype.push.apply(params, filter_params);

      param_idx += filter_params.length;
    }

    // TODO restrict fields returned based on projection
    text.push(`returning ${this.#join[0].relation.path}.*`);

    return {
      sql: text.join(' '),
      params
    };
  }

  delete() {
    const text = [`delete from ${this.#join[0].relation.path}`];
    const params = [];
    let param_idx = 1;

    if (this.#join.length > 1) {
      text.push(`using ${this.#join[1].relation.path}`);

      for (let jidx = 2; jidx < this.#join.length; jidx++) {
        const join = this.#join[jidx];
        const {
          sql: join_sql,
          params: join_params
        } = join.on.compile(param_idx);

        if (join.relation instanceof Alias) {
          text.push(`${join.type.description} ${join.relation.path} as "${join.relation.name}" on ${join_sql}`);
        } else {
          text.push(`${join.type.description} ${join.relation.path} on ${join_sql}`);
        }
        Array.prototype.push.apply(params, join_params);

        param_idx += join_params.length;
      }

      const {
        sql: first_join_sql,
        params: first_join_params
      } = this.#join[1].on.compile(param_idx); // first non-origin relation!

      param_idx += first_join_params.length;
      Array.prototype.push.apply(params, first_join_params);

      text.push(`where ${first_join_sql}`);
    }

    if (this.#filter && this.#filter.is_active) {
      text.push(this.#join.length > 1 ? 'and' : 'where');

      const {
        sql: filter_sql,
        params: filter_params
      } = this.#filter.compile(param_idx);

      text.push(filter_sql);

      Array.prototype.push.apply(params, filter_params);

      param_idx += filter_params.length;
    }

    // TODO restrict fields returned based on projection
    text.push(`returning ${this.#join[0].relation.path}.*`);

    return {
      sql: text.join(' '),
      params
    };
  }
}

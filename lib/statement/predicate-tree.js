import pgp from 'pg-promise';
import lex from './lex.js';
import Expr from './expr.js';
import Statement from './index.js';

import quote from '../util/quote.js';

const mutators = {
  identity: (lhs, op, rhs, param_idx) => {
    if (param_idx === undefined) {
      // column-to-column comparison
      return {
        sql: pgp.as.format(`${lhs} ${op} ${rhs}`)
      };
    }

    return {
      sql: pgp.as.format(`${lhs} ${op} $${param_idx}`),
      params: [rhs]
    };
  },
  equality: (lhs, op, rhs, param_idx) => {
    if (op === '=' && (rhs === null || Object.prototype.toString.call(rhs) === '[object Boolean]')) {
      // nulls must be tested with `is`! booleans can use equality but it's nicer
      op = 'is';
    }

    if (param_idx === undefined) {
      // column-to-column comparison
      return {
        sql: pgp.as.format(`${lhs} ${op} ${rhs}`)
      };
    }

    if (Array.isArray(rhs)) {
      // build an `in`
      // TODO if no-length rhs substitute `any('{}')` (equals) or `all` (neq)
      const placeholders = rhs.map((_, idx) => `$${param_idx + idx}`);

      return {
        sql: pgp.as.format(`${lhs} in (${placeholders.join(', ')})`),
        params: rhs
      }
    }

    return {
      sql: pgp.as.format(`${lhs} ${op} $${param_idx}`),
      params: [rhs]
    };
  }
};

// generate a reversed token tree of operations, e.g. "to" -> "similar" -> ["not" -> "is", "is"]
// leaves are mutators, so we can search the tree by tokens from the end of a
// key and find the appropriate mutator to apply
const operations = [
  // equality and comparison
  ['=', mutators.equality],
  ['!=', mutators.equality],
  ['<>', mutators.equality],
  ['>'],
  ['>='],
  ['<'],
  ['<='],
  ['between', mutators.between],
  ['is'],
  ['is', 'not'],
  ['is', 'distinct', 'from'],
  ['is', 'not', 'distinct', 'from'],

  // patterns
  ['~~'],
  ['like'],
  ['!~~'],
  ['not', 'like'],
  ['~~*'],
  ['ilike'],
  ['!~~*'],
  ['not', 'ilike'],

  // regex
  ['similar', 'to'],
  ['not', 'similar', 'to'],
  ['~'],
  ['!~'],
  ['~*'],
  ['!~*'],

  // array
  ['@>', mutators.array],
  ['<@', mutators.array],
  ['&&', mutators.array],

  // json
  ['?'],
  ['?|', mutators.array],
  ['?&', mutators.array],
  ['@?'],
  ['@@'],

  // existence
  // TODO implement; subqueries currently only support `in`
  ['exists', mutators.subquery],
  ['not', 'exists', mutators.subquery]

  // TODO postgis
].reduce((acc, operation) => {
  const fn = typeof operation[operation.length - 1] === 'function' ? operation.pop() : mutators.identity;
  let val, acc_pointer = acc;

  while (val = operation.pop()) {
    if (!acc_pointer[val]) {
      acc_pointer[val] = {};
    }

    if (operation.length === 0) {
      acc_pointer[val] = fn;
    } else {
      acc_pointer = acc_pointer[val];
    }
  }

  return acc;
}, {});

export default class PredicateTree {
  #booleans = [];
  #criteria = [];
  #exprs = [];
  #known_columns = [];

  constructor(known_columns) {
    this.#known_columns = known_columns;
  };

  add(...criteria) {
    for (const c of criteria) {
      if (c instanceof Expr) {
        this.#exprs.push(pgp.as.format(c.sql, c.params));
      } else if (Object.prototype.toString.call(c) === '[object String]') {
        this.#exprs.push(c);
      } else if (Object.prototype.toString.call(c) === '[object Object]') {
        this.#criteria.push(c);
      } else if (Object.prototype.toString.call(c) === '[object Boolean]') {
        this.#booleans.push(true)
      } else {
        throw new Error(`expected criteria object, expr, string, or boolean; got ${Object.prototype.toString.call(c)}`);
      }
    }
  }

  get is_active() {
    return this.#criteria.length > 0 || this.#exprs.length > 0 || this.#booleans.length > 0;
  }

  clone() {
    const clone = new PredicateTree(this.#known_columns);

    clone.#exprs = [...this.#exprs];
    clone.#criteria = [...this.#criteria];
    clone.#booleans = [...this.#booleans];

    return clone;
  }

  /**
   * Extract from the join criteria a map of fields in rel (usually the same
   * relation joined by this predicate tree) and corresponding fields in other
   * relations.
   */
  involved_fields(rel) {
    return this.#criteria.reduce((map, criterion) => {
      for (const [key, val] of Object.entries(criterion)) {
        if (key.startsWith(rel.path) || (rel.name && key.startsWith(quote(rel.name)))) {
          map[key] = val;
        } else {
          map[val] = key;
        }
      }

      return map;
    }, {});
  }

  compile(param_idx) {
    const compiled_sql = [];
    const compiled_params = [];

    for (const b of this.#booleans) {
      compiled_sql.push(b);
    }

    for (const c of this.#criteria) {
      const {sql, params} = this.conjoin(c, param_idx);

      compiled_sql.push(sql);
      Array.prototype.push.apply(compiled_params, params);
      param_idx += params.length;
    }

    for (const e of this.#exprs) {
      compiled_sql.push(e);
    }

    return {
      sql: compiled_sql.join(' and '),
      params: compiled_params
    };
  }

  conjoin(criterion, param_idx) {
    const conjunction_sql = [];
    const conjunction_params = [];
    let sql, params;

    for (const key in criterion) {
      switch (key) {
        case '$and':
          criterion[key].forEach(c => {
            ({sql, params} = this.conjoin(c, param_idx));

            conjunction_sql.push(sql);
            Array.prototype.push.apply(conjunction_params, params);
            param_idx += params && params.length || 0;
          });
          break;

        case '$or':
          ({sql, params} = this.disjoin(criterion[key], param_idx));

          conjunction_sql.push(sql);
          Array.prototype.push.apply(conjunction_params, params);
          param_idx += params && params.length || 0;
          break;

        default:
          const tokens = lex(key);
          let op_tokens = 0;
          let mutator;

          if (tokens.length > 1) {
            let idx = tokens.length - 1;

            do {
              if (!operations[tokens[idx]]) break;

              mutator = operations[tokens[idx]];

              op_tokens++;
            } while (idx-- > 0);
          }

          if (mutator) {
            const lhs = tokens.slice(0, op_tokens === 0 ? tokens.length : -op_tokens);
            const op = op_tokens === 0 ? [] : tokens.slice(-op_tokens);
            ({sql, params} = mutator(
              lhs.join(' '),
              op.join(' '),
              criterion[key],
              this.#known_columns.indexOf(criterion[key]) === -1 ? param_idx : undefined
            ));
          } else if (criterion[key] instanceof Statement) {
            ({
              sql,
              params
            } = criterion[key].select());

            sql = pgp.as.format(`${tokens} in (${sql})`);
          } else {
            ({sql, params} = mutators.equality(
              tokens.join(' '),
              '=',
              criterion[key],
              this.#known_columns.indexOf(criterion[key]) === -1 ? param_idx : undefined
            ));
          }

          conjunction_sql.push(sql);
          Array.prototype.push.apply(conjunction_params, params);
          param_idx += params && params.length || 0;

          break;
      }
    }

    return {
      sql: conjunction_sql.join(' and '),
      params: conjunction_params
    };
  }

  disjoin(criterion, param_idx) {
    const disjunction_sql = [];
    const disjunction_params = [];
    let sql, params;

    for (const disjunct of criterion) {
      ({sql, params} = this.conjoin(disjunct, param_idx));

      disjunction_sql.push(sql);
      Array.prototype.push.apply(disjunction_params, params);
      param_idx += params && params.length || 0;
    }

    return {
      sql: disjunction_sql.join(' or '),
      params: disjunction_params
    };
  }
};

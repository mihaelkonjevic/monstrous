export default function unquote(str) {
  if (str.startsWith('"') && str.endsWith('"')) return str;

  return `"${str}"`;
}

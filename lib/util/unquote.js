export default function unquote(str) {
  const start = str[0] === '"' ? 1 : 0;
  const end = str[str.length - 1] === '"' ? -1 : str.length;

  return str.slice(start, end);
}

/* IMPORTANT
 * This snapshot file is auto-generated, but designed for humans.
 * It should be checked into source control and tracked carefully.
 * Re-generate by setting TAP_SNAPSHOT=1 and running tests.
 * Make sure to inspect the output below.  Do not ignore changes!
 */
'use strict'
exports[`test/database/target/unit.js TAP returns a single value for db.$target.unit > matches query text 1`] = `
select "regular_table"."txt" as "txt" from "regular_table" where "regular_table"."id" = $1 limit 1
`

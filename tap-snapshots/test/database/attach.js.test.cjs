/* IMPORTANT
 * This snapshot file is auto-generated, but designed for humans.
 * It should be checked into source control and tracked carefully.
 * Re-generate by setting TAP_SNAPSHOT=1 and running tests.
 * Make sure to inspect the output below.  Do not ignore changes!
 */
'use strict'
exports[`test/database/attach.js TAP attach a statement > matches query result 1`] = `
Array [
  Object {
    "bool": true,
    "id": 1,
    "inserted_at": {time},
    "num": 1,
    "txt": "one",
  },
]
`

exports[`test/database/attach.js TAP attach a statement > matches query text 1`] = `
select "regular_table"."id" as "id", "regular_table"."bool" as "bool", "regular_table"."num" as "num", "regular_table"."txt" as "txt", "regular_table"."inserted_at" as "inserted_at" from "regular_table" where num <= 2.5 limit 1
`

exports[`test/database/attach.js TAP attach a statement deep in the tree > matches query result 1`] = `
Array [
  Object {
    "bool": true,
    "id": 1,
    "inserted_at": {time},
    "num": 1,
    "txt": "one",
  },
]
`

exports[`test/database/attach.js TAP attach a statement deep in the tree > matches query text 1`] = `
select "regular_table"."id" as "id", "regular_table"."bool" as "bool", "regular_table"."num" as "num", "regular_table"."txt" as "txt", "regular_table"."inserted_at" as "inserted_at" from "regular_table" where num <= 2.5 limit 1
`

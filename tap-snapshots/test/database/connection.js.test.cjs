/* IMPORTANT
 * This snapshot file is auto-generated, but designed for humans.
 * It should be checked into source control and tracked carefully.
 * Re-generate by setting TAP_SNAPSHOT=1 and running tests.
 * Make sure to inspect the output below.  Do not ignore changes!
 */
'use strict'
exports[`test/database/connection.js TAP commits transactions > must match snapshot 1`] = `
Array [
  Object {
    "bool": null,
    "id": 7,
    "inserted_at": {time},
    "num": null,
    "txt": "jkl",
  },
]
`

exports[`test/database/connection.js TAP runs tasks on a single connection > must match snapshot 1`] = `
Array [
  Object {
    "bool": null,
    "id": 6,
    "inserted_at": {time},
    "num": null,
    "txt": "def",
  },
]
`

exports[`test/database/connection.js TAP saves: inserts > inserts new item 1`] = `
Object {
  "bool": null,
  "id": 9,
  "inserted_at": {time},
  "num": null,
  "txt": "new",
}
`

exports[`test/database/connection.js TAP saves: inserts > matches query text 1`] = `
insert into "regular_table" (txt) values ($1) returning *
`

exports[`test/database/connection.js TAP saves: updates > matches query text 1`] = `
update "regular_table" set txt = $1 where id = $2 returning "regular_table".*
`

exports[`test/database/connection.js TAP saves: updates > updates existing item 1`] = `
Object {
  "bool": true,
  "id": 1,
  "inserted_at": {time},
  "num": 1,
  "txt": "saved",
}
`

/* IMPORTANT
 * This snapshot file is auto-generated, but designed for humans.
 * It should be checked into source control and tracked carefully.
 * Re-generate by setting TAP_SNAPSHOT=1 and running tests.
 * Make sure to inspect the output below.  Do not ignore changes!
 */
'use strict'
exports[`test/database/executable.js TAP run a queryfile > matches results 1`] = `
Array [
  Object {
    "column1": 1,
    "column2": "one",
  },
  Object {
    "column1": 2,
    "column2": "two",
  },
  Object {
    "column1": 3,
    "column2": "three",
  },
]
`

exports[`test/database/executable.js TAP run a queryfile with named parameters > matches results 1`] = `
Array [
  Object {
    "one": 1,
    "two": 2,
  },
]
`

exports[`test/database/executable.js TAP supply the \`one\` target to a queryfile > matches results 1`] = `
Object {
  "arg": "text",
  "num": 1,
}
`

exports[`test/database/executable.js TAP supply the \`unit\` target to a queryfile > matches results 1`] = `
1
`

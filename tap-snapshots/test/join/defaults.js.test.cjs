/* IMPORTANT
 * This snapshot file is auto-generated, but designed for humans.
 * It should be checked into source control and tracked carefully.
 * Re-generate by setting TAP_SNAPSHOT=1 and running tests.
 * Make sure to inspect the output below.  Do not ignore changes!
 */
'use strict'
exports[`test/join/defaults.js TAP autogenerates keys deeper in the join tree > matches query text 1`] = `
select "foreign_keys"."alpha"."id" as "id", "foreign_keys"."alpha"."val" as "val", "foreign_keys"."beta"."id" as "beta__id", "foreign_keys"."beta"."alpha_id" as "beta__alpha_id", "foreign_keys"."beta"."val" as "beta__val", "foreign_keys"."beta"."j" as "beta__j", "foreign_keys_too"."delta"."id" as "beta__delta__id", "foreign_keys_too"."delta"."beta_id" as "beta__delta__beta_id", "foreign_keys_too"."delta"."val" as "beta__delta__val" from "foreign_keys"."alpha" inner join "foreign_keys"."beta" on "foreign_keys"."beta"."alpha_id" = "foreign_keys"."alpha"."id" inner join "foreign_keys_too"."delta" on "foreign_keys_too"."delta"."beta_id" = "foreign_keys"."beta"."id" where "foreign_keys"."alpha"."id" > $1
`

exports[`test/join/defaults.js TAP autogenerates keys deeper in the join tree > matches results 1`] = `
Array [
  Object {
    "beta": Array [
      Object {
        "alpha_id": 2,
        "delta": Array [
          Object {
            "beta_id": 2,
            "id": 2,
            "val": "beta two",
          },
        ],
        "id": 2,
        "j": null,
        "val": "alpha two",
      },
    ],
    "id": 2,
    "val": "two",
  },
]
`

exports[`test/join/defaults.js TAP autogenerates keys when one possible join fk matches > matches query text 1`] = `
select "foreign_keys"."alpha"."id" as "id", "foreign_keys"."alpha"."val" as "val", "foreign_keys"."beta"."id" as "beta__id", "foreign_keys"."beta"."alpha_id" as "beta__alpha_id", "foreign_keys"."beta"."val" as "beta__val", "foreign_keys"."beta"."j" as "beta__j" from "foreign_keys"."alpha" inner join "foreign_keys"."beta" on "foreign_keys"."beta"."alpha_id" = "foreign_keys"."alpha"."id" where "foreign_keys"."alpha"."id" = $1
`

exports[`test/join/defaults.js TAP autogenerates keys when one possible join fk matches > matches results 1`] = `
Array [
  Object {
    "beta": Array [
      Object {
        "alpha_id": 3,
        "id": 3,
        "j": null,
        "val": "alpha three",
      },
      Object {
        "alpha_id": 3,
        "id": 4,
        "j": null,
        "val": "alpha three again",
      },
    ],
    "id": 3,
    "val": "three",
  },
]
`

exports[`test/join/defaults.js TAP autogenerates keys when one possible origin fk matches > matches query text 1`] = `
select "foreign_keys"."beta"."id" as "id", "foreign_keys"."beta"."alpha_id" as "alpha_id", "foreign_keys"."beta"."val" as "val", "foreign_keys"."beta"."j" as "j", "foreign_keys"."alpha"."id" as "alpha__id", "foreign_keys"."alpha"."val" as "alpha__val" from "foreign_keys"."beta" inner join "foreign_keys"."alpha" on "foreign_keys"."beta"."alpha_id" = "foreign_keys"."alpha"."id" where "foreign_keys"."alpha"."id" = $1
`

exports[`test/join/defaults.js TAP autogenerates keys when one possible origin fk matches > matches results 1`] = `
Array [
  Object {
    "alpha": Array [
      Object {
        "id": 3,
        "val": "three",
      },
    ],
    "alpha_id": 3,
    "id": 3,
    "j": null,
    "val": "alpha three",
  },
  Object {
    "alpha": Array [
      Object {
        "id": 3,
        "val": "three",
      },
    ],
    "alpha_id": 3,
    "id": 4,
    "j": null,
    "val": "alpha three again",
  },
]
`

exports[`test/join/defaults.js TAP defaults to inner joins > matches query text 1`] = `
select "foreign_keys"."alpha"."id" as "id", "foreign_keys"."alpha"."val" as "val", "foreign_keys"."beta"."id" as "beta__id", "foreign_keys"."beta"."alpha_id" as "beta__alpha_id", "foreign_keys"."beta"."val" as "beta__val", "foreign_keys"."beta"."j" as "beta__j" from "foreign_keys"."alpha" inner join "foreign_keys"."beta" on "foreign_keys"."beta"."alpha_id" = "foreign_keys"."alpha"."id" where "foreign_keys"."alpha"."id" > $1
`

exports[`test/join/defaults.js TAP defaults to inner joins > matches results 1`] = `
Array [
  Object {
    "beta": Array [
      Object {
        "alpha_id": 2,
        "id": 2,
        "j": null,
        "val": "alpha two",
      },
    ],
    "id": 2,
    "val": "two",
  },
  Object {
    "beta": Array [
      Object {
        "alpha_id": 3,
        "id": 3,
        "j": null,
        "val": "alpha three",
      },
      Object {
        "alpha_id": 3,
        "id": 4,
        "j": null,
        "val": "alpha three again",
      },
    ],
    "id": 3,
    "val": "three",
  },
]
`

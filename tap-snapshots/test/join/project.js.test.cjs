/* IMPORTANT
 * This snapshot file is auto-generated, but designed for humans.
 * It should be checked into source control and tracked carefully.
 * Re-generate by setting TAP_SNAPSHOT=1 and running tests.
 * Make sure to inspect the output below.  Do not ignore changes!
 */
'use strict'
exports[`test/join/project.js TAP adds exprs to the join projection array > matches query text 1`] = `
select "foreign_keys"."beta"."id" as "id", "foreign_keys"."alpha"."id" + 1 as "aplus", "foreign_keys"."beta"."id" + 1 as "bplus" from "foreign_keys"."alpha" inner join "foreign_keys"."beta" on "foreign_keys"."beta"."alpha_id" = "foreign_keys"."alpha"."id" where "foreign_keys"."alpha"."id" = $1
`

exports[`test/join/project.js TAP adds exprs to the join projection array > matches results 1`] = `
Array [
  Object {
    "aplus": 4,
    "bplus": 4,
    "id": 3,
  },
  Object {
    "aplus": 4,
    "bplus": 5,
    "id": 4,
  },
]
`

exports[`test/join/project.js TAP adds exprs to the join projection object > matches query text 1`] = `
select "foreign_keys"."alpha"."id" as "id", "foreign_keys"."alpha"."id" + 1 as "aplus", ("foreign_keys"."alpha"."id" + 1, 'some text') as "tuple", "foreign_keys"."beta"."id" as "beta__id", "foreign_keys"."beta"."alpha_id" as "beta__alpha_id", "foreign_keys"."beta"."val" as "beta__val", "foreign_keys"."beta"."j" as "beta__j", "foreign_keys"."beta"."id" + 1 as "beta__bplus" from "foreign_keys"."alpha" inner join "foreign_keys"."beta" on "foreign_keys"."beta"."alpha_id" = "foreign_keys"."alpha"."id" where "foreign_keys"."alpha"."id" = $1
`

exports[`test/join/project.js TAP adds exprs to the join projection object > matches results 1`] = `
Array [
  Object {
    "aplus": 4,
    "beta": Array [
      Object {
        "alpha_id": 3,
        "bplus": 4,
        "id": 3,
        "j": null,
        "val": "alpha three",
      },
      Object {
        "alpha_id": 3,
        "bplus": 5,
        "id": 4,
        "j": null,
        "val": "alpha three again",
      },
    ],
    "id": 3,
  },
]
`

exports[`test/join/project.js TAP joins from a view with an explicit fully specified projection > matches query text 1`] = `
select "foreign_keys"."beta_view"."id" as "id", "foreign_keys"."beta_view"."alpha_id" as "alpha_id", "foreign_keys"."beta_view"."j" as "j", "foreign_keys"."beta_view"."val" as "val", "foreign_keys"."alpha"."id" as "alpha__id", "foreign_keys"."alpha"."val" as "alpha__val" from "foreign_keys"."beta_view" inner join "foreign_keys"."alpha" on "foreign_keys"."alpha"."id" = "foreign_keys"."beta_view"."alpha_id" where "foreign_keys"."alpha"."id" = $1
`

exports[`test/join/project.js TAP joins from a view with an explicit fully specified projection > matches results 1`] = `
Array [
  Object {
    "alpha": Array [
      Object {
        "id": 3,
        "val": "three",
      },
    ],
    "alpha_id": 3,
    "id": 3,
    "j": null,
    "val": "alpha three",
  },
  Object {
    "alpha": Array [
      Object {
        "id": 3,
        "val": "three",
      },
    ],
    "alpha_id": 3,
    "id": 4,
    "j": null,
    "val": "alpha three again",
  },
]
`

exports[`test/join/project.js TAP joins from a view with an explicit projection using $columns > matches query text 1`] = `
select "foreign_keys"."beta_view"."id" as "id", "foreign_keys"."beta_view"."alpha_id" as "alpha_id", "foreign_keys"."beta_view"."val" as "val", "foreign_keys"."beta_view"."j" as "j", "foreign_keys"."alpha"."id" as "alpha__id", "foreign_keys"."alpha"."val" as "alpha__val" from "foreign_keys"."beta_view" inner join "foreign_keys"."alpha" on "foreign_keys"."alpha"."id" = "foreign_keys"."beta_view"."alpha_id" where "foreign_keys"."alpha"."id" = $1
`

exports[`test/join/project.js TAP joins from a view with an explicit projection using $columns > matches results 1`] = `
Array [
  Object {
    "alpha": Array [
      Object {
        "id": 3,
        "val": "three",
      },
    ],
    "alpha_id": 3,
    "id": 3,
    "j": null,
    "val": "alpha three",
  },
  Object {
    "alpha": Array [
      Object {
        "id": 3,
        "val": "three",
      },
    ],
    "alpha_id": 3,
    "id": 4,
    "j": null,
    "val": "alpha three again",
  },
]
`

exports[`test/join/project.js TAP omits a child relation from the final result > matches query text 1`] = `
select "foreign_keys"."alpha"."id" as "id", "foreign_keys"."alpha"."val" as "val" from "foreign_keys"."alpha" left outer join "foreign_keys"."alpha_zeta" on "foreign_keys"."alpha_zeta"."alpha_id" = "foreign_keys"."alpha"."id" where "foreign_keys"."alpha_zeta"."zeta_id" in ($1, $2)
`

exports[`test/join/project.js TAP omits a child relation from the final result > matches results 1`] = `
Array [
  Object {
    "id": 1,
    "val": "one",
  },
  Object {
    "id": 1,
    "val": "one",
  },
]
`

exports[`test/join/project.js TAP omits a junction relation from the final result > matches query text 1`] = `
select "foreign_keys"."alpha"."id" as "id", "foreign_keys"."alpha"."val" as "val", "foreign_keys"."zeta"."id" as "zeta__id", "foreign_keys"."zeta"."val" as "zeta__val" from "foreign_keys"."alpha" left outer join "foreign_keys"."alpha_zeta" on "foreign_keys"."alpha_zeta"."alpha_id" = "foreign_keys"."alpha"."id" left outer join "foreign_keys"."zeta" on "foreign_keys"."zeta"."id" = "foreign_keys"."alpha_zeta"."zeta_id" where "foreign_keys"."alpha"."id" in ($1, $2)
`

exports[`test/join/project.js TAP omits a junction relation from the final result > matches results 1`] = `
Array [
  Object {
    "id": 1,
    "val": "one",
    "zeta": Array [
      Object {
        "id": 1,
        "val": "alpha one",
      },
      Object {
        "id": 2,
        "val": "alpha one again",
      },
    ],
  },
  Object {
    "id": 3,
    "val": "three",
    "zeta": Array [],
  },
]
`

exports[`test/join/project.js TAP omits a parent relation from the final result > matches query text 1`] = `
select "foreign_keys"."alpha_zeta"."alpha_id" as "alpha_id", "foreign_keys"."alpha_zeta"."zeta_id" as "zeta_id", "foreign_keys"."zeta"."id" as "zeta__id", "foreign_keys"."zeta"."val" as "zeta__val" from "foreign_keys"."alpha" left outer join "foreign_keys"."alpha_zeta" on "foreign_keys"."alpha_zeta"."alpha_id" = "foreign_keys"."alpha"."id" left outer join "foreign_keys"."zeta" on "foreign_keys"."zeta"."id" = "foreign_keys"."alpha_zeta"."zeta_id" where "foreign_keys"."alpha"."id" in ($1, $2)
`

exports[`test/join/project.js TAP omits a parent relation from the final result > matches results 1`] = `
Array [
  Object {
    "alpha_id": 1,
    "zeta": Array [
      Object {
        "id": 1,
        "val": "alpha one",
      },
      Object {
        "id": 2,
        "val": "alpha one again",
      },
    ],
    "zeta_id": 2,
  },
  Object {
    "alpha_id": 2,
    "zeta": Array [
      Object {
        "id": 3,
        "val": "alpha two",
      },
    ],
    "zeta_id": 3,
  },
]
`

exports[`test/join/project.js TAP renames a relation > matches query text 1`] = `
select "foreign_keys"."alpha"."id" as "id", "foreign_keys"."alpha"."val" as "val", "foreign_keys"."beta"."id" as "beta_but_better__id", "foreign_keys"."beta"."alpha_id" as "beta_but_better__alpha_id", "foreign_keys"."beta"."val" as "beta_but_better__val", "foreign_keys"."beta"."j" as "beta_but_better__j" from "foreign_keys"."alpha" inner join "foreign_keys"."beta" on "foreign_keys"."beta"."alpha_id" = "foreign_keys"."alpha"."id" where "foreign_keys"."alpha"."id" in ($1, $2)
`

exports[`test/join/project.js TAP renames a relation > matches results 1`] = `
Array [
  Object {
    "beta_but_better": Array [
      Object {
        "alpha_id": 1,
        "id": 1,
        "j": null,
        "val": "alpha one",
      },
    ],
    "id": 1,
    "val": "one",
  },
  Object {
    "beta_but_better": Array [
      Object {
        "alpha_id": 3,
        "id": 3,
        "j": null,
        "val": "alpha three",
      },
      Object {
        "alpha_id": 3,
        "id": 4,
        "j": null,
        "val": "alpha three again",
      },
    ],
    "id": 3,
    "val": "three",
  },
]
`

exports[`test/join/project.js TAP uses primary_key alongside qualified columns > matches query text 1`] = `
select "foreign_keys"."alpha"."id" as "id", "foreign_keys"."alpha"."val" as "val", "foreign_keys"."zeta"."id" as "zeta__id", "foreign_keys"."zeta"."val" as "zeta__val" from "foreign_keys"."alpha" left outer join "foreign_keys"."alpha_zeta" on "foreign_keys"."alpha_zeta"."alpha_id" = "foreign_keys"."alpha"."id" left outer join "foreign_keys"."zeta" on "foreign_keys"."zeta"."id" = "foreign_keys"."alpha_zeta"."zeta_id" where "foreign_keys"."alpha"."id" in ($1, $2)
`

exports[`test/join/project.js TAP uses primary_key alongside qualified columns > matches results 1`] = `
Array [
  Object {
    "id": 1,
    "val": "one",
    "zeta": Array [
      Object {
        "id": 1,
        "val": "alpha one",
      },
      Object {
        "id": 2,
        "val": "alpha one again",
      },
    ],
  },
  Object {
    "id": 3,
    "val": "three",
    "zeta": Array [],
  },
]
`

exports[`test/join/project.js TAP uses primary_key instead of qualified columns > matches query text 1`] = `
select "foreign_keys"."alpha"."id" as "id", "foreign_keys"."zeta"."id" as "zeta__id", "foreign_keys"."zeta"."val" as "zeta__val" from "foreign_keys"."alpha" left outer join "foreign_keys"."alpha_zeta" on "foreign_keys"."alpha_zeta"."alpha_id" = "foreign_keys"."alpha"."id" left outer join "foreign_keys"."zeta" on "foreign_keys"."zeta"."id" = "foreign_keys"."alpha_zeta"."zeta_id" where "foreign_keys"."alpha"."id" in ($1, $2)
`

exports[`test/join/project.js TAP uses primary_key instead of qualified columns > matches results 1`] = `
Array [
  Object {
    "id": 1,
    "zeta": Array [
      Object {
        "id": 1,
        "val": "alpha one",
      },
      Object {
        "id": 2,
        "val": "alpha one again",
      },
    ],
  },
  Object {
    "id": 3,
    "zeta": Array [],
  },
]
`

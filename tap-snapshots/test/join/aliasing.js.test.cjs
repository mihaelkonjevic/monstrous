/* IMPORTANT
 * This snapshot file is auto-generated, but designed for humans.
 * It should be checked into source control and tracked carefully.
 * Re-generate by setting TAP_SNAPSHOT=1 and running tests.
 * Make sure to inspect the output below.  Do not ignore changes!
 */
'use strict'
exports[`test/join/aliasing.js TAP aliases to the table name when processing an implicit relation in another schema > matches query text 1`] = `
select "foreign_keys"."alpha"."id" as "id", "foreign_keys"."alpha"."val" as "val", "foreign_keys_too"."epsilon"."id" as "epsilon__id", "foreign_keys_too"."epsilon"."alpha_id" as "epsilon__alpha_id", "foreign_keys_too"."epsilon"."val" as "epsilon__val" from "foreign_keys"."alpha" inner join "foreign_keys_too"."epsilon" on "foreign_keys_too"."epsilon"."alpha_id" = "foreign_keys"."alpha"."id"
`

exports[`test/join/aliasing.js TAP aliases to the table name when processing an implicit relation in another schema > matches results 1`] = `
Array [
  Object {
    "epsilon": Array [
      Object {
        "alpha_id": 1,
        "id": 1,
        "val": "alpha one",
      },
    ],
    "id": 1,
    "val": "one",
  },
]
`

exports[`test/join/aliasing.js TAP allows self joins with an alias > matches query text 1`] = `
select "foreign_keys"."alpha"."id" as "id", "foreign_keys"."alpha"."val" as "val", "alpha_again"."id" as "alpha_again__id", "alpha_again"."val" as "alpha_again__val" from "foreign_keys"."alpha" inner join "foreign_keys"."alpha" as "alpha_again" on "foreign_keys"."alpha"."id" = "alpha_again"."id" where "foreign_keys"."alpha"."id" = $1
`

exports[`test/join/aliasing.js TAP allows self joins with an alias > matches results 1`] = `
Array [
  Object {
    "alpha_again": Array [
      Object {
        "id": 1,
        "val": "one",
      },
    ],
    "id": 1,
    "val": "one",
  },
]
`

exports[`test/join/aliasing.js TAP defers to an explicit relation but aliases to the key > joins parent and child tables, with child aliased 1`] = `
Array [
  Object {
    "asdf": Array [
      Object {
        "alpha_id": 3,
        "id": 3,
        "j": null,
        "val": "alpha three",
      },
      Object {
        "alpha_id": 3,
        "id": 4,
        "j": null,
        "val": "alpha three again",
      },
    ],
    "id": 3,
    "val": "three",
  },
]
`

exports[`test/join/aliasing.js TAP defers to an explicit relation but aliases to the key > matches query text 1`] = `
select "foreign_keys"."alpha"."id" as "id", "foreign_keys"."alpha"."val" as "val", "asdf"."id" as "asdf__id", "asdf"."alpha_id" as "asdf__alpha_id", "asdf"."val" as "asdf__val", "asdf"."j" as "asdf__j" from "foreign_keys"."alpha" inner join "foreign_keys"."beta" as "asdf" on "asdf"."alpha_id" = "foreign_keys"."alpha"."id" where "foreign_keys"."alpha"."id" = $1
`

exports[`test/join/aliasing.js TAP joins the same table multiple times under different aliases > matches query text 1`] = `
select "foreign_keys"."gamma"."id" as "id", "foreign_keys"."gamma"."alpha_id_one" as "alpha_id_one", "foreign_keys"."gamma"."alpha_id_two" as "alpha_id_two", "foreign_keys"."gamma"."beta_id" as "beta_id", "foreign_keys"."gamma"."val" as "val", "foreign_keys"."gamma"."j" as "j", "alpha1"."id" as "alpha1__id", "alpha1"."val" as "alpha1__val", "alpha2"."id" as "alpha2__id", "alpha2"."val" as "alpha2__val" from "foreign_keys"."gamma" inner join "foreign_keys"."alpha" as "alpha1" on "alpha1"."id" = "foreign_keys"."gamma"."alpha_id_one" inner join "foreign_keys"."alpha" as "alpha2" on "alpha2"."id" = "foreign_keys"."gamma"."alpha_id_two" where "alpha1"."id" = $1
`

exports[`test/join/aliasing.js TAP joins the same table multiple times under different aliases > matches results 1`] = `
Array [
  Object {
    "alpha_id_one": 3,
    "alpha_id_two": 1,
    "alpha1": Array [
      Object {
        "id": 3,
        "val": "three",
      },
    ],
    "alpha2": Array [
      Object {
        "id": 1,
        "val": "one",
      },
    ],
    "beta_id": 4,
    "id": 5,
    "j": null,
    "val": "alpha three alpha one beta four",
  },
]
`

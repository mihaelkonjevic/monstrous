/* IMPORTANT
 * This snapshot file is auto-generated, but designed for humans.
 * It should be checked into source control and tracked carefully.
 * Re-generate by setting TAP_SNAPSHOT=1 and running tests.
 * Make sure to inspect the output below.  Do not ignore changes!
 */
'use strict'
exports[`test/join/subqueries.js TAP joins a subquery > joins parent and child tables 1`] = `
Array [
  Object {
    "id": 1,
    "join_child": Array [
      Object {
        "id": 1,
        "parent_id": 1,
        "txt": "one-one",
      },
    ],
    "txt": "one",
  },
]
`

exports[`test/join/subqueries.js TAP joins a subquery > matches query text 1`] = `
select "join_parent"."id" as "id", "join_parent"."txt" as "txt", "join_child"."id" as "join_child__id", "join_child"."parent_id" as "join_child__parent_id", "join_child"."txt" as "join_child__txt" from "join_parent" inner join (select "join_child"."id" as "id", "join_child"."parent_id" as "parent_id", "join_child"."txt" as "txt" from "join_child" where id = $1) as "join_child" on "join_child"."parent_id" = "join_parent"."id"
`

exports[`test/join/subqueries.js TAP joins a subquery for insert but does nothing with it > joins parent and child tables 1`] = `
Array [
  Object {
    "id": 4,
    "txt": "what now?",
  },
]
`

exports[`test/join/subqueries.js TAP joins a subquery for insert but does nothing with it > matches query text 1`] = `
with join_parent_cte as (insert into "join_parent" (txt) values ($1) returning *)  select * from join_parent_cte
`

exports[`test/join/subqueries.js TAP joins a subquery, aliased > joins parent and child tables 1`] = `
Array [
  Object {
    "id": 1,
    "jc": Array [
      Object {
        "id": 1,
        "parent_id": 1,
        "txt": "one-one",
      },
    ],
    "txt": "one",
  },
]
`

exports[`test/join/subqueries.js TAP joins a subquery, aliased > matches query text 1`] = `
select "join_parent"."id" as "id", "join_parent"."txt" as "txt", "jc"."id" as "jc__id", "jc"."parent_id" as "jc__parent_id", "jc"."txt" as "jc__txt" from "join_parent" inner join (select "join_child"."id" as "id", "join_child"."parent_id" as "parent_id", "join_child"."txt" as "txt" from "join_child" where id = $1) as "jc" on "jc"."parent_id" = "join_parent"."id"
`

exports[`test/join/subqueries.js TAP joins a subquery, inline, aliased, qualified > joins parent and child tables 1`] = `
Array [
  Object {
    "id": 1,
    "jc": Array [
      Object {
        "id": 1,
        "parent_id": 1,
        "txt": "one-one",
      },
    ],
    "txt": "one",
  },
]
`

exports[`test/join/subqueries.js TAP joins a subquery, inline, aliased, qualified > matches query text 1`] = `
select "join_parent"."id" as "id", "join_parent"."txt" as "txt", "jc"."id" as "jc__id", "jc"."parent_id" as "jc__parent_id", "jc"."txt" as "jc__txt" from "join_parent" inner join (select "join_child"."id" as "id", "join_child"."parent_id" as "parent_id", "join_child"."txt" as "txt" from "join_child" where id = $1) as "jc" on jc.parent_id = "join_parent"."id"
`

exports[`test/join/subqueries.js TAP joins a subquery, inline, aliased, unqualified > joins parent and child tables 1`] = `
Array [
  Object {
    "id": 1,
    "jc": Array [
      Object {
        "id": 1,
        "parent_id": 1,
        "txt": "one-one",
      },
    ],
    "txt": "one",
  },
]
`

exports[`test/join/subqueries.js TAP joins a subquery, inline, aliased, unqualified > matches query text 1`] = `
select "join_parent"."id" as "id", "join_parent"."txt" as "txt", "jc"."id" as "jc__id", "jc"."parent_id" as "jc__parent_id", "jc"."txt" as "jc__txt" from "join_parent" inner join (select "join_child"."id" as "id", "join_child"."parent_id" as "parent_id", "join_child"."txt" as "txt" from "join_child" where id = $1) as "jc" on parent_id = "join_parent"."id"
`

exports[`test/join/subqueries.js TAP joins a subquery, inline, unaliased, unqualified > joins parent and child tables 1`] = `
Array [
  Object {
    "id": 1,
    "join_child": Array [
      Object {
        "id": 1,
        "parent_id": 1,
        "txt": "one-one",
      },
    ],
    "txt": "one",
  },
]
`

exports[`test/join/subqueries.js TAP joins a subquery, inline, unaliased, unqualified > matches query text 1`] = `
select "join_parent"."id" as "id", "join_parent"."txt" as "txt", "join_child"."id" as "join_child__id", "join_child"."parent_id" as "join_child__parent_id", "join_child"."txt" as "join_child__txt" from "join_parent" inner join (select "join_child"."id" as "id", "join_child"."parent_id" as "parent_id", "join_child"."txt" as "txt" from "join_child" where id = $1) as "join_child" on parent_id = "join_parent"."id"
`

/* IMPORTANT
 * This snapshot file is auto-generated, but designed for humans.
 * It should be checked into source control and tracked carefully.
 * Re-generate by setting TAP_SNAPSHOT=1 and running tests.
 * Make sure to inspect the output below.  Do not ignore changes!
 */
'use strict'
exports[`test/join/tree.js TAP joins involving a joined relation as well as the origin > matches query text 1`] = `
select "foreign_keys"."alpha"."id" as "id", "foreign_keys"."alpha"."val" as "val", "foreign_keys"."beta"."id" as "beta__id", "foreign_keys"."beta"."alpha_id" as "beta__alpha_id", "foreign_keys"."beta"."val" as "beta__val", "foreign_keys"."beta"."j" as "beta__j", "foreign_keys"."gamma"."id" as "beta__gamma__id", "foreign_keys"."gamma"."alpha_id_one" as "beta__gamma__alpha_id_one", "foreign_keys"."gamma"."alpha_id_two" as "beta__gamma__alpha_id_two", "foreign_keys"."gamma"."beta_id" as "beta__gamma__beta_id", "foreign_keys"."gamma"."val" as "beta__gamma__val", "foreign_keys"."gamma"."j" as "beta__gamma__j" from "foreign_keys"."alpha" left outer join "foreign_keys"."beta" on "foreign_keys"."beta"."alpha_id" = "foreign_keys"."alpha"."id" left outer join "foreign_keys"."gamma" on "foreign_keys"."gamma"."alpha_id_one" = "foreign_keys"."alpha"."id" and "foreign_keys"."gamma"."beta_id" <> "foreign_keys"."beta"."id" where "foreign_keys"."alpha"."id" > $1
`

exports[`test/join/tree.js TAP joins involving a joined relation as well as the origin > matches results 1`] = `
Array [
  Object {
    "beta": Array [
      Object {
        "alpha_id": 2,
        "gamma": Array [
          Object {
            "alpha_id_one": 2,
            "alpha_id_two": null,
            "beta_id": 3,
            "id": 4,
            "j": null,
            "val": "alpha two (alpha null) beta three",
          },
        ],
        "id": 2,
        "j": null,
        "val": "alpha two",
      },
    ],
    "id": 2,
    "val": "two",
  },
  Object {
    "beta": Array [
      Object {
        "alpha_id": 3,
        "gamma": Array [
          Object {
            "alpha_id_one": 3,
            "alpha_id_two": 1,
            "beta_id": 4,
            "id": 5,
            "j": null,
            "val": "alpha three alpha one beta four",
          },
        ],
        "id": 3,
        "j": null,
        "val": "alpha three",
      },
      Object {
        "alpha_id": 3,
        "gamma": Array [],
        "id": 4,
        "j": null,
        "val": "alpha three again",
      },
    ],
    "id": 3,
    "val": "three",
  },
  Object {
    "beta": Array [],
    "id": 4,
    "val": "four",
  },
]
`

exports[`test/join/tree.js TAP joins multiple tables at multiple levels > matches query text 1`] = `
select "foreign_keys"."alpha"."id" as "id", "foreign_keys"."alpha"."val" as "val", "foreign_keys"."beta"."id" as "beta__id", "foreign_keys"."beta"."alpha_id" as "beta__alpha_id", "foreign_keys"."beta"."val" as "beta__val", "foreign_keys"."beta"."j" as "beta__j", "foreign_keys"."gamma"."id" as "beta__gamma__id", "foreign_keys"."gamma"."alpha_id_one" as "beta__gamma__alpha_id_one", "foreign_keys"."gamma"."alpha_id_two" as "beta__gamma__alpha_id_two", "foreign_keys"."gamma"."beta_id" as "beta__gamma__beta_id", "foreign_keys"."gamma"."val" as "beta__gamma__val", "foreign_keys"."gamma"."j" as "beta__gamma__j", "foreign_keys_too"."delta"."id" as "beta__delta__id", "foreign_keys_too"."delta"."beta_id" as "beta__delta__beta_id", "foreign_keys_too"."delta"."val" as "beta__delta__val", "foreign_keys_too"."epsilon"."id" as "epsilon__id", "foreign_keys_too"."epsilon"."alpha_id" as "epsilon__alpha_id", "foreign_keys_too"."epsilon"."val" as "epsilon__val" from "foreign_keys"."alpha" inner join "foreign_keys"."beta" on "foreign_keys"."beta"."alpha_id" = "foreign_keys"."alpha"."id" inner join "foreign_keys"."gamma" on "foreign_keys"."gamma"."beta_id" = "foreign_keys"."beta"."id" inner join "foreign_keys_too"."delta" on "foreign_keys_too"."delta"."beta_id" = "foreign_keys"."beta"."id" inner join "foreign_keys_too"."epsilon" on "foreign_keys_too"."epsilon"."alpha_id" = "foreign_keys"."alpha"."id" where "foreign_keys"."alpha"."id" = $1
`

exports[`test/join/tree.js TAP joins multiple tables at multiple levels > matches results 1`] = `
Array [
  Object {
    "beta": Array [
      Object {
        "alpha_id": 1,
        "delta": Array [
          Object {
            "beta_id": 1,
            "id": 1,
            "val": "beta one",
          },
        ],
        "gamma": Array [
          Object {
            "alpha_id_one": 1,
            "alpha_id_two": 1,
            "beta_id": 1,
            "id": 1,
            "j": null,
            "val": "alpha one alpha one beta one",
          },
        ],
        "id": 1,
        "j": null,
        "val": "alpha one",
      },
    ],
    "epsilon": Array [
      Object {
        "alpha_id": 1,
        "id": 1,
        "val": "alpha one",
      },
    ],
    "id": 1,
    "val": "one",
  },
]
`

exports[`test/join/tree.js TAP joins to a joined relation instead of the origin > matches query text 1`] = `
select "foreign_keys"."alpha"."id" as "id", "foreign_keys"."alpha"."val" as "val", "foreign_keys"."beta"."id" as "beta__id", "foreign_keys"."beta"."alpha_id" as "beta__alpha_id", "foreign_keys"."beta"."val" as "beta__val", "foreign_keys"."beta"."j" as "beta__j", "foreign_keys"."gamma"."id" as "beta__gamma__id", "foreign_keys"."gamma"."alpha_id_one" as "beta__gamma__alpha_id_one", "foreign_keys"."gamma"."alpha_id_two" as "beta__gamma__alpha_id_two", "foreign_keys"."gamma"."beta_id" as "beta__gamma__beta_id", "foreign_keys"."gamma"."val" as "beta__gamma__val", "foreign_keys"."gamma"."j" as "beta__gamma__j" from "foreign_keys"."alpha" inner join "foreign_keys"."beta" on "foreign_keys"."beta"."alpha_id" = "foreign_keys"."alpha"."id" inner join "foreign_keys"."gamma" on "foreign_keys"."gamma"."beta_id" = "foreign_keys"."beta"."id" where "foreign_keys"."alpha"."id" > $1
`

exports[`test/join/tree.js TAP joins to a joined relation instead of the origin > matches results 1`] = `
Array [
  Object {
    "beta": Array [
      Object {
        "alpha_id": 2,
        "gamma": Array [
          Object {
            "alpha_id_one": 1,
            "alpha_id_two": 2,
            "beta_id": 2,
            "id": 2,
            "j": null,
            "val": "alpha two alpha two beta two",
          },
          Object {
            "alpha_id_one": 2,
            "alpha_id_two": 3,
            "beta_id": 2,
            "id": 3,
            "j": null,
            "val": "alpha two alpha three beta two again",
          },
        ],
        "id": 2,
        "j": null,
        "val": "alpha two",
      },
    ],
    "id": 2,
    "val": "two",
  },
  Object {
    "beta": Array [
      Object {
        "alpha_id": 3,
        "gamma": Array [
          Object {
            "alpha_id_one": 2,
            "alpha_id_two": null,
            "beta_id": 3,
            "id": 4,
            "j": null,
            "val": "alpha two (alpha null) beta three",
          },
        ],
        "id": 3,
        "j": null,
        "val": "alpha three",
      },
      Object {
        "alpha_id": 3,
        "gamma": Array [
          Object {
            "alpha_id_one": 3,
            "alpha_id_two": 1,
            "beta_id": 4,
            "id": 5,
            "j": null,
            "val": "alpha three alpha one beta four",
          },
        ],
        "id": 4,
        "j": null,
        "val": "alpha three again",
      },
    ],
    "id": 3,
    "val": "three",
  },
]
`

exports[`test/join/tree.js TAP joins to a joined relation instead of the origin, through an implicit foreign key relationship > matches query text 1`] = `
select "foreign_keys"."alpha"."id" as "id", "foreign_keys"."alpha"."val" as "val", "foreign_keys"."beta"."id" as "beta__id", "foreign_keys"."beta"."alpha_id" as "beta__alpha_id", "foreign_keys"."beta"."val" as "beta__val", "foreign_keys"."beta"."j" as "beta__j", "foreign_keys_too"."delta"."id" as "beta__delta__id", "foreign_keys_too"."delta"."beta_id" as "beta__delta__beta_id", "foreign_keys_too"."delta"."val" as "beta__delta__val" from "foreign_keys"."alpha" inner join "foreign_keys"."beta" on "foreign_keys"."beta"."alpha_id" = "foreign_keys"."alpha"."id" inner join "foreign_keys_too"."delta" on "foreign_keys_too"."delta"."beta_id" = "foreign_keys"."beta"."id" where "foreign_keys"."alpha"."id" > $1
`

exports[`test/join/tree.js TAP joins to a joined relation instead of the origin, through an implicit foreign key relationship > matches results 1`] = `
Array [
  Object {
    "beta": Array [
      Object {
        "alpha_id": 2,
        "delta": Array [
          Object {
            "beta_id": 2,
            "id": 2,
            "val": "beta two",
          },
        ],
        "id": 2,
        "j": null,
        "val": "alpha two",
      },
    ],
    "id": 2,
    "val": "two",
  },
]
`

exports[`test/join/tree.js TAP joins to a self-joining table > matches query text 1`] = `
select "foreign_keys"."selfjoin"."id" as "id", "foreign_keys"."selfjoin"."beta_id" as "beta_id", "foreign_keys"."selfjoin"."parent_id" as "parent_id", "foreign_keys"."selfjoin"."val" as "val", "sj"."id" as "sj__id", "sj"."beta_id" as "sj__beta_id", "sj"."parent_id" as "sj__parent_id", "sj"."val" as "sj__val" from "foreign_keys"."selfjoin" inner join "foreign_keys"."selfjoin" as "sj" on "sj"."parent_id" = "foreign_keys"."selfjoin"."id"
`

exports[`test/join/tree.js TAP joins to a self-joining table > matches results 1`] = `
Array [
  Object {
    "beta_id": 1,
    "id": 1,
    "parent_id": null,
    "sj": Array [
      Object {
        "beta_id": 2,
        "id": 2,
        "parent_id": 1,
        "val": "two",
      },
      Object {
        "beta_id": null,
        "id": 3,
        "parent_id": 1,
        "val": "three",
      },
    ],
    "val": "one",
  },
]
`

exports[`test/join/tree.js TAP joins to a self-joining table, from another > matches query text 1`] = `
select "foreign_keys"."beta"."id" as "id", "foreign_keys"."beta"."alpha_id" as "alpha_id", "foreign_keys"."beta"."val" as "val", "foreign_keys"."beta"."j" as "j", "sj1"."id" as "sj1__id", "sj1"."beta_id" as "sj1__beta_id", "sj1"."parent_id" as "sj1__parent_id", "sj1"."val" as "sj1__val", "sj2"."id" as "sj1__sj2__id", "sj2"."beta_id" as "sj1__sj2__beta_id", "sj2"."parent_id" as "sj1__sj2__parent_id", "sj2"."val" as "sj1__sj2__val" from "foreign_keys"."beta" inner join "foreign_keys"."selfjoin" as "sj1" on "sj1"."beta_id" = "foreign_keys"."beta"."id" inner join "foreign_keys"."selfjoin" as "sj2" on "sj2"."parent_id" = "sj1"."id"
`

exports[`test/join/tree.js TAP joins to a self-joining table, from another > matches results 1`] = `
Array [
  Object {
    "alpha_id": 1,
    "id": 1,
    "j": null,
    "sj1": Array [
      Object {
        "beta_id": 1,
        "id": 1,
        "parent_id": null,
        "sj2": Array [
          Object {
            "beta_id": 2,
            "id": 2,
            "parent_id": 1,
            "val": "two",
          },
          Object {
            "beta_id": null,
            "id": 3,
            "parent_id": 1,
            "val": "three",
          },
        ],
        "val": "one",
      },
    ],
    "val": "alpha one",
  },
]
`

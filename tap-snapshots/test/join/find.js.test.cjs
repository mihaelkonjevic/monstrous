/* IMPORTANT
 * This snapshot file is auto-generated, but designed for humans.
 * It should be checked into source control and tracked carefully.
 * Re-generate by setting TAP_SNAPSHOT=1 and running tests.
 * Make sure to inspect the output below.  Do not ignore changes!
 */
'use strict'
exports[`test/join/find.js TAP returns a linear subtree from db.$target.one > matches query text 1`] = `
select "foreign_keys"."alpha"."id" as "id", "foreign_keys"."alpha"."val" as "val", "foreign_keys"."beta"."id" as "beta__id", "foreign_keys"."beta"."alpha_id" as "beta__alpha_id", "foreign_keys"."beta"."val" as "beta__val", "foreign_keys"."beta"."j" as "beta__j" from "foreign_keys"."alpha" inner join "foreign_keys"."beta" on "foreign_keys"."beta"."alpha_id" = "foreign_keys"."alpha"."id" where "foreign_keys"."alpha"."id" = $1 limit 1
`

exports[`test/join/find.js TAP returns a linear subtree from db.$target.one > matches results 1`] = `
Object {
  "beta": Array [
    Object {
      "alpha_id": 3,
      "id": 3,
      "j": null,
      "val": "alpha three",
    },
  ],
  "id": 3,
  "val": "three",
}
`

exports[`test/join/find.js TAP returns a linear subtree from db.$target.one with a primitive pk > matches query text 1`] = `
select "foreign_keys"."alpha"."id" as "id", "foreign_keys"."alpha"."val" as "val", "foreign_keys"."beta"."id" as "beta__id", "foreign_keys"."beta"."alpha_id" as "beta__alpha_id", "foreign_keys"."beta"."val" as "beta__val", "foreign_keys"."beta"."j" as "beta__j" from "foreign_keys"."alpha" inner join "foreign_keys"."beta" on "foreign_keys"."beta"."alpha_id" = "foreign_keys"."alpha"."id" where "foreign_keys"."alpha"."id" = $1 limit 1
`

exports[`test/join/find.js TAP returns a linear subtree from db.$target.one with a primitive pk > matches results 1`] = `
Object {
  "beta": Array [
    Object {
      "alpha_id": 3,
      "id": 3,
      "j": null,
      "val": "alpha three",
    },
  ],
  "id": 3,
  "val": "three",
}
`

exports[`test/join/find.js TAP returns an array from find with a primitive pk > matches query text 1`] = `
select "foreign_keys"."alpha"."id" as "id", "foreign_keys"."alpha"."val" as "val", "foreign_keys"."beta"."id" as "beta__id", "foreign_keys"."beta"."alpha_id" as "beta__alpha_id", "foreign_keys"."beta"."val" as "beta__val", "foreign_keys"."beta"."j" as "beta__j" from "foreign_keys"."alpha" inner join "foreign_keys"."beta" on "foreign_keys"."beta"."alpha_id" = "foreign_keys"."alpha"."id" where "foreign_keys"."alpha"."id" = $1
`

exports[`test/join/find.js TAP returns an array from find with a primitive pk > matches results 1`] = `
Array [
  Object {
    "beta": Array [
      Object {
        "alpha_id": 3,
        "id": 3,
        "j": null,
        "val": "alpha three",
      },
      Object {
        "alpha_id": 3,
        "id": 4,
        "j": null,
        "val": "alpha three again",
      },
    ],
    "id": 3,
    "val": "three",
  },
]
`

/* IMPORTANT
 * This snapshot file is auto-generated, but designed for humans.
 * It should be checked into source control and tracked carefully.
 * Re-generate by setting TAP_SNAPSHOT=1 and running tests.
 * Make sure to inspect the output below.  Do not ignore changes!
 */
'use strict'
exports[`test/statement/first.js TAP sets and executes the appropriate qrm > matches query result 1`] = `
Object {
  "bool": true,
  "id": 1,
  "inserted_at": {time},
  "num": 1,
  "txt": "one",
}
`

exports[`test/statement/first.js TAP sets and executes the appropriate qrm > matches query text 1`] = `
select "regular_table"."bool" as "bool", "regular_table"."id" as "id", "regular_table"."inserted_at" as "inserted_at", "regular_table"."num" as "num", "regular_table"."txt" as "txt" from "regular_table" limit 1
`

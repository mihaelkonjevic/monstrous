/* IMPORTANT
 * This snapshot file is auto-generated, but designed for humans.
 * It should be checked into source control and tracked carefully.
 * Re-generate by setting TAP_SNAPSHOT=1 and running tests.
 * Make sure to inspect the output below.  Do not ignore changes!
 */
'use strict'
exports[`test/statement/group.js TAP group by and aggregate > matches query result 1`] = `
Array [
  Object {
    "bool": false,
    "count": "2",
  },
  Object {
    "bool": true,
    "count": "3",
  },
]
`

exports[`test/statement/group.js TAP group by and aggregate > matches query text 1`] = `
select "regular_table"."bool" as "bool", count(*) as "count" from "regular_table" group by "regular_table"."bool"
`

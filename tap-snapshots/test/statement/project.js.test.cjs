/* IMPORTANT
 * This snapshot file is auto-generated, but designed for humans.
 * It should be checked into source control and tracked carefully.
 * Re-generate by setting TAP_SNAPSHOT=1 and running tests.
 * Make sure to inspect the output below.  Do not ignore changes!
 */
'use strict'
exports[`test/statement/project.js TAP accepts and uses key arrays > must match snapshot 1`] = `
Array [
  Object {
    "children1": Array [
      Object {
        "children2": Array [
          Object {
            "id_one": 21,
            "id_two": 22,
            "val": "d1",
          },
        ],
        "id_one": 11,
        "id_two": 12,
        "val": "c1",
      },
      Object {
        "children2": Array [
          Object {
            "id_one": 23,
            "id_two": 24,
            "val": "d2",
          },
          Object {
            "id_one": 25,
            "id_two": 26,
            "val": "d3",
          },
        ],
        "id_one": 13,
        "id_two": 14,
        "val": "c2",
      },
    ],
    "id_one": 1,
    "id_two": 2,
    "val": "p1",
  },
  Object {
    "children1": Array [
      Object {
        "children2": Array [
          Object {
            "id_one": 27,
            "id_two": 28,
            "val": "d4",
          },
        ],
        "id_one": 15,
        "id_two": 16,
        "val": "c3",
      },
    ],
    "id_one": 3,
    "id_two": 4,
    "val": "p2",
  },
]
`

exports[`test/statement/project.js TAP applies new parents only in the correct scope > must match snapshot 1`] = `
Array [
  Object {
    "account": Object {
      "id": 1,
    },
    "address": Object {
      "city": "Sao Paulo",
      "complement": null,
      "coords": Object {
        "latitude": "1",
        "longitude": "2",
      },
      "coords__latitude": "1",
      "coords__longitude": "2",
      "neighborhood": null,
      "number": "number",
      "state": "Sao Paulo",
      "street": "street",
      "zipCode": "zip",
    },
    "contact": Object {
      "email": "email",
      "phone": "phone",
    },
    "labels": Array [
      Object {
        "color": "yellow",
        "id": "297726d0-301d-4de6-b9a4-e439b81f44ba",
        "name": "Contrato",
        "type": 1,
      },
      Object {
        "color": "purple",
        "id": "1db6e07f-91e2-42fb-b65c-9a364b6bad4c",
        "name": "Particular",
        "type": 1,
      },
    ],
    "this_archived": false,
    "this_id": 1,
    "this_name": "Eduardo Luiz",
    "this_notes": null,
  },
]
`

exports[`test/statement/project.js TAP articulates array fields > must match snapshot 1`] = `
Array [
  Object {
    "arr": Array [
      "one",
      "two",
    ],
    "children": Array [
      Object {
        "arr": Array [
          "three",
          "four",
        ],
        "id": 11,
      },
    ],
    "id": 1,
  },
]
`

exports[`test/statement/project.js TAP articulates empty child arrays from null children > must match snapshot 1`] = `
Array [
  Object {
    "children": Array [],
    "id": 1,
    "val": "p1",
  },
  Object {
    "children": Array [
      Object {
        "id": 11,
        "val": "c1",
      },
    ],
    "id": 2,
    "val": "p2",
  },
]
`

exports[`test/statement/project.js TAP articulates empty child arrays from null children at any level > must match snapshot 1`] = `
Array [
  Object {
    "children": Array [
      Object {
        "grandchildren": Array [
          Object {
            "id": 111,
            "val": "g1",
          },
          Object {
            "id": 112,
            "val": "g2",
          },
        ],
        "id": 11,
        "val": "c1",
      },
      Object {
        "grandchildren": Array [
          Object {
            "id": 121,
            "val": "g3",
          },
        ],
        "id": 12,
        "val": "c2",
      },
    ],
    "id": 1,
    "val": "p1",
  },
  Object {
    "children": Array [],
    "id": 2,
    "val": "p2",
  },
  Object {
    "children": Array [
      Object {
        "grandchildren": Array [],
        "id": 31,
        "val": "c3",
      },
      Object {
        "grandchildren": Array [
          Object {
            "id": 321,
            "val": "g4",
          },
        ],
        "id": 32,
        "val": "c4",
      },
    ],
    "id": 3,
    "val": "p3",
  },
]
`

exports[`test/statement/project.js TAP articulates objects > must match snapshot 1`] = `
Array [
  Object {
    "children": Object {
      "id": 11,
      "val": "c1",
    },
    "id": 1,
    "val": "p1",
  },
]
`

exports[`test/statement/project.js TAP articulates partial results > must match snapshot 1`] = `
Array [
  Object {
    "children": Array [
      Object {
        "id": 11,
        "val": undefined,
      },
    ],
    "id": 1,
    "val": undefined,
  },
]
`

exports[`test/statement/project.js TAP collapses children into other children > must match snapshot 1`] = `
Array [
  Object {
    "children1": Array [
      Object {
        "children2": Array [
          Object {
            "id": 21,
            "val": "d1",
          },
        ],
        "id": 11,
        "val": "c1",
      },
      Object {
        "children2": Array [
          Object {
            "id": 22,
            "val": "d2",
          },
          Object {
            "id": 23,
            "val": "d3",
          },
        ],
        "id": 12,
        "val": "c2",
      },
    ],
    "id": 1,
    "val": "p1",
  },
]
`

exports[`test/statement/project.js TAP collapses multiple children with the same parent > must match snapshot 1`] = `
Array [
  Object {
    "children1": Array [
      Object {
        "id": 11,
        "val": "c1",
      },
      Object {
        "id": 12,
        "val": "c2",
      },
    ],
    "children2": Array [
      Object {
        "id": 21,
        "val": "d1",
      },
      Object {
        "id": 22,
        "val": "d2",
      },
      Object {
        "id": 23,
        "val": "d3",
      },
    ],
    "id": 1,
    "val": "p1",
  },
]
`

exports[`test/statement/project.js TAP collapses object descendants > must match snapshot 1`] = `
Array [
  Object {
    "child": Object {
      "grandchild": Object {
        "id": 111,
        "val": "g1",
      },
      "id": 11,
      "val": "c1",
    },
    "id": 1,
    "val": "p1",
  },
]
`

exports[`test/statement/project.js TAP collapses simple tree structures > must match snapshot 1`] = `
Array [
  Object {
    "children": Array [
      Object {
        "id": 11,
        "val": "c1",
      },
      Object {
        "id": 12,
        "val": "c2",
      },
    ],
    "id": 1,
    "val": "p1",
  },
]
`

exports[`test/statement/project.js TAP consolidates duplicate children by pk > must match snapshot 1`] = `
Array [
  Object {
    "children": Array [
      Object {
        "id": 11,
        "val": "c1",
      },
      Object {
        "id": 12,
        "val": "c2",
      },
    ],
    "id": 1,
    "val": "p1",
  },
]
`

exports[`test/statement/project.js TAP last object wins in the case of accidental object articulation > must match snapshot 1`] = `
Array [
  Object {
    "children": Object {
      "id": 12,
      "val": "c2",
    },
    "id": 1,
    "val": "p1",
  },
]
`

exports[`test/statement/project.js TAP omits null object descendants > must match snapshot 1`] = `
Array [
  Object {
    "child": Object {
      "id": 11,
      "val": "c1",
    },
    "id": 1,
    "val": "p1",
  },
]
`

exports[`test/statement/project.js TAP returns empty if given empty data and non-empty schema > must match snapshot 1`] = `
Array []
`

exports[`test/statement/project.js TAP returns empty if given empty data and schema > must match snapshot 1`] = `
Array []
`

exports[`test/statement/project.js TAP sorts children according to its own whims > must match snapshot 1`] = `
Array [
  Object {
    "children": Array [
      Object {
        "id": 11,
        "val": "c2",
      },
      Object {
        "id": 12,
        "val": "c1",
      },
    ],
    "id": 1,
    "val": "p1",
  },
]
`

exports[`test/statement/project.js TAP sorts children according to its own whims > must match snapshot 2`] = `
Array [
  Object {
    "children": Array [
      Object {
        "id": 11,
        "val": "c2",
      },
      Object {
        "id": 12,
        "val": "c1",
      },
    ],
    "id": 1,
    "val": "p1",
  },
]
`

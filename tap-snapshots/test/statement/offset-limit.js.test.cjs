/* IMPORTANT
 * This snapshot file is auto-generated, but designed for humans.
 * It should be checked into source control and tracked carefully.
 * Re-generate by setting TAP_SNAPSHOT=1 and running tests.
 * Make sure to inspect the output below.  Do not ignore changes!
 */
'use strict'
exports[`test/statement/offset-limit.js TAP limits > matches query text 1`] = `
select "regular_table"."id" as "id", "regular_table"."bool" as "bool", "regular_table"."num" as "num", "regular_table"."txt" as "txt", "regular_table"."inserted_at" as "inserted_at" from "regular_table" limit 2
`

exports[`test/statement/offset-limit.js TAP limits > retrieves some rows 1`] = `
Array [
  Object {
    "bool": true,
    "id": 1,
    "inserted_at": {time},
    "num": 1,
    "txt": "one",
  },
  Object {
    "bool": false,
    "id": 2,
    "inserted_at": {time},
    "num": 2.1,
    "txt": "two",
  },
]
`

exports[`test/statement/offset-limit.js TAP offsets > matches query text 1`] = `
select "regular_table"."id" as "id", "regular_table"."bool" as "bool", "regular_table"."num" as "num", "regular_table"."txt" as "txt", "regular_table"."inserted_at" as "inserted_at" from "regular_table" offset 2
`

exports[`test/statement/offset-limit.js TAP offsets > retrieves tailing rows 1`] = `
Array [
  Object {
    "bool": true,
    "id": 3,
    "inserted_at": {time},
    "num": 3.2,
    "txt": "three",
  },
  Object {
    "bool": false,
    "id": 4,
    "inserted_at": {time},
    "num": 4.3,
    "txt": "four",
  },
  Object {
    "bool": true,
    "id": 5,
    "inserted_at": {time},
    "num": 5.4,
    "txt": "five",
  },
]
`

exports[`test/statement/offset-limit.js TAP offsets and limits > matches query text 1`] = `
select "regular_table"."id" as "id", "regular_table"."bool" as "bool", "regular_table"."num" as "num", "regular_table"."txt" as "txt", "regular_table"."inserted_at" as "inserted_at" from "regular_table" offset 2 limit 1
`

exports[`test/statement/offset-limit.js TAP offsets and limits > retrieves offset and limited rows 1`] = `
Array [
  Object {
    "bool": true,
    "id": 3,
    "inserted_at": {time},
    "num": 3.2,
    "txt": "three",
  },
]
`
